// noinspection ES6UnusedImports
import webpack, { Configuration } from 'webpack';
import BaseChain, { LoaderOptions } from 'webpack-chain';
import { join, resolve } from 'path';
import { spawn } from 'child_process';
import { Options as TSLoaderOptions } from 'ts-loader';
import { TransformOptions } from '@babel/core';
import tsImport from 'ts-import-plugin';

type TypescriptLoaderOptions = LoaderOptions & TSLoaderOptions

interface BabelLoaderOptions extends TransformOptions, LoaderOptions {
    customize?: string
    cacheDirectory?: boolean
    cacheCompression?: boolean
}



class Chain extends BaseChain {
    toConfigHandlers = [];

    onToConfig(handler: (this: this, config?: Configuration) => Configuration) {
        this.toConfigHandlers.push(handler);
        return this;
    }

    public toConfig(): Configuration {
        let config = super.toConfig();
        if ( this.toConfigHandlers ) {
            this.toConfigHandlers.forEach(handler => {
                config = handler(config);
            });
        }
        return config;
    }
}

const port     = parseInt(process.env.PORT || '3000');
const mode     = 'development';
const isDev    = mode === 'development';
const tsconfig = resolve(__dirname, '../tsconfig.json');
const chain    = new Chain();

chain.mode(mode);

// chain.module.rule('tsx')
//     .test(/\.tsx?$/)
//     .exclude.add(/node_modules/).end()
//     .use('react-hot-loader').loader('react-hot-loader/webpack').end()
//     .use('ts-loader')
//     .loader('ts-loader')
//     .options(<LoaderOptions & TSLoaderOptions>{
//         configFile     : tsconfig,
//         compilerOptions: {
//             noEmit: false,
//         },
//     })
//     .end();

chain.output
    .filename('bundle.js')
    .libraryTarget('commonjs2');

chain.resolve
    .symlinks(true)
    .extensions.merge([ '.js', '.ts', '.tsx', '.json' ]).end()
// .extensions.merge([ '.js', '.vue', '.json', '.web.ts', '.ts', '.web.tsx', '.tsx', '.styl', '.less', '.scss', '.stylus', '.css', '.mjs', '.web.js', '.json', '.web.jsx', '.jsx' ]).end()
    .mainFields.merge([ 'module', 'browser', 'main' ]).end() // 'jsnext:main',
    .mainFiles.merge([ 'index', 'index.ts', 'index.tsx' ]).end()
    .modules.merge([ 'node_modules' ]).end()
    .alias.merge({
    'lodash-es$': 'lodash',
}).end();

chain.module.rule('ts').test(/\.(ts|tsx)$/);
addTsToRule(chain, 'ts', {});

chain.when(isDev, chain => {
    chain
        .devtool('inline-source-map')
        .target('electron-renderer');
    chain
        .entry('app')
        .merge([
            'react-hot-loader/patch',
            `webpack-dev-server/client?http://localhost:${port}/`,
            'webpack/hot/only-dev-server',
            resolve(__dirname, '../src/index.tsx'),
        ]);
    chain.output
        .publicPath(`http://localhost:${port}/`)
        .filename('renderer.dev.js');

    chain.node
        .set('__dirname', false)
        .set('__filename', false);

    chain.plugin('define').use(webpack.DefinePlugin, [ {
        'process.env.NODE_ENV': JSON.stringify('development'),
    } ]);
    chain.plugin('loader').use(webpack.LoaderOptionsPlugin, [ {
        debug: true,
    } ]);

    chain.devServer
        .port(port)
        .publicPath(chain.output.get('publicPath'))
        .compress(true)
        // .noInfo(true)
        .quiet(false)
        .stats('normal')
        .inline(true)
        .lazy(false)
        .hot(true)
        .headers({ 'Access-Control-Allow-Origin': '*' })
        .contentBase(join(__dirname, '../public'))
        .watchOptions({
            aggregateTimeout: 300,
            ignored         : /node_modules/,
            poll            : 100,
        })
        .historyApiFallback({
            verbose       : true,
            disableDotRule: false,
        })
        .set('before', () => {
            if ( process.env.START_HOT ) {
                console.log('Starting Main Process...');
                spawn('npm', [ 'run', 'start-main-dev' ], {
                    shell: true,
                    env  : process.env,
                    stdio: 'inherit',
                })
                    .on('close', code => process.exit(code))
                    .on('error', spawnError => console.error(spawnError));
            }
        });


    chain.onToConfig(config => addStyleRules(config))
    addHMR(chain);

});


export function addBabelToRule(chain: Chain, ruleName: string, options: BabelLoaderOptions = {}) {
    let rule = chain.module.rule(ruleName);
    rule.use('babel-loader')
        .loader('babel-loader')
        .options(<BabelLoaderOptions>{
            babelrc       : false,
            configFile    : false,
            presets       : [
                [ 'react-app' ],
            ],
            plugins       : [
                // 'jsx-control-statements',
                // [ 'react-css-modules', {
                //     'context'               : resolve(__dirname, '../src'),
                //     'filetypes'             : {
                //         '.mscss': {
                //             'syntax' : 'postcss-scss',
                //             'plugins': [
                //                 'postcss-nested',
                //             ],
                //         },
                //     },
                //     'handleMissingStyleName': 'warn',
                //     'generateScopedName'    : '[name]__[local]',
                // } ],
                [ 'import', { libraryName: 'antd', style: true }, 'import-antd' ],
            ].filter(Boolean),
            cacheDirectory: true,
            compact       : false,
            ...options,
        } as any);
}

export function addTsToRule(chain: Chain, ruleName: string, options: Partial<TypescriptLoaderOptions> = {}, babelOptions: BabelLoaderOptions = {}) {
    let rule = chain.module.rule(ruleName);
    if ( !rule.has('babel-loader') ) {
        addBabelToRule(chain, ruleName, babelOptions);
    }
    rule
        .use('ts-loader')
        .loader('ts-loader')
        .options(<Partial<TypescriptLoaderOptions>>{
            transpileOnly        : true,
            configFile           : tsconfig,
            // happyPackMode        : true,
            getCustomTransformers: () => ({
                before: [
                    tsImport([
                        { libraryName: 'antd', style: true },
                    ]) as any,
                ],
            }),
            ...options,
        } as any);
}

export function addHMR(chain: Chain, reactHotLoader: boolean = true) {
    chain.plugin('hmr').use(webpack.HotModuleReplacementPlugin, [ {
        multiStep: true,
    } ]);
    chain.resolve.alias.set('react-dom', '@hot-loader/react-dom');
    return;
    const modifyOptions = (o: BabelLoaderOptions) => {
        if ( reactHotLoader ) {
            o.plugins.push('react-hot-loader/babel');
        }
        let reactCssModulesIndex = o.plugins.findIndex(plugin => Array.isArray(plugin) && plugin[ 0 ] === 'react-css-modules');
        if ( reactCssModulesIndex !== - 1 ) {
            o.plugins[ reactCssModulesIndex ][ 1 ].webpackHotModuleReloading = true;
        }
        return o;
    };
    chain.module.rule('js').use('babel-loader').tap(modifyOptions);
    chain.module.rule('ts').use('babel-loader').tap(modifyOptions);
}

export function addStyleRules(config: webpack.Configuration) {
    config.module.rules.push(...[
        {
            test   : /\.global\.css$/,
            loaders: [
                'style-loader',
                'css-loader?sourceMap',
            ],
        },

        {
            test   : /^((?!\.global).)*\.css$/,
            loaders: [
                'style-loader',
                'css-loader?modules&sourceMap&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
            ],
        },

        // Add SASS support  - compile all .global.scss files and pipe it to style.css
        {
            test: /\.global\.scss$/,
            use : [
                {
                    loader: 'style-loader',
                },
                {
                    loader : 'css-loader',
                    options: {
                        sourceMap: true,
                    },
                },
                {
                    loader: 'sass-loader',
                },
            ],
        },
        // Add SASS support  - compile all other .scss files and pipe it to style.css
        {
            test: /^((?!\.global).)*\.scss$/,
            use : [
                {
                    loader: 'style-loader',
                },
                {
                    loader : 'css-loader',
                    options: {
                        modules       : true,
                        sourceMap     : true,
                        importLoaders : 1,
                        localIdentName: '[name]__[local]__[hash:base64:5]',
                    },
                },
                {
                    loader: 'sass-loader',
                },
            ],
        },
        {
            test: /\.less$/,
            use : [
                { loader: 'style-loader' },
                {
                    loader : 'css-loader',
                    options: {
                        modules       : true,
                        sourceMap     : true,
                        importLoaders : 1,
                        localIdentName: '[name]__[local]__[hash:base64:5]',
                    },
                },
                {
                    loader : 'less-loader',
                    options: {
                        javascriptEnabled: true,
                        modifyVars       : {
                            'hack': `true; @import "${resolve(__dirname, '../src/styling/antd.less')}";`,
                        },
                    },
                },
            ],
        },

        // WOFF Font
        {
            test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
            use : {
                loader : 'url-loader',
                options: {
                    limit   : 10000,
                    mimetype: 'application/font-woff',
                },
            },
        },
        // WOFF2 Font
        {
            test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
            use : {
                loader : 'url-loader',
                options: {
                    limit   : 10000,
                    mimetype: 'application/font-woff',
                },
            },
        },
        // TTF Font
        {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            use : {
                loader : 'url-loader',
                options: {
                    limit   : 10000,
                    mimetype: 'application/octet-stream',
                },
            },
        },
        // EOT Font
        {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            use : 'file-loader',
        },
        // SVG Font
        {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            use : {
                loader : 'url-loader',
                options: {
                    limit   : 10000,
                    mimetype: 'image/svg+xml',
                },
            },
        },
        // Common Image Formats
        {
            test: /\.(?:ico|gif|png|jpg|jpeg|webp)$/,
            use : 'url-loader',
        },
    ]);
    return config;
}


const config = chain.toConfig();

export { chain, config };
export default config;