import React from 'react';
import { Layout } from 'antd';


const { Header, Sider, Content, Footer } = Layout;
const App: React.FC                      = () => {

    return (
        <div>

            <Layout style={{ minHeight: '100vh' }}>
                <Header>Header</Header>
                <Layout>
                    <Sider>Sider</Sider>
                    <Content
                        style={{
                            margin   : '16px',
                            padding  : 16,
                            minHeight: 280,
                        }}
                    >
                        <pre>
                            <code>
                            </code>
                        </pre>

                    </Content>
                </Layout>
                <Footer>Footer</Footer>
            </Layout>

        </div>
    );
};

export default App;
