// noinspection ES6UnusedImports
import electron, { app, App, BrowserWindow, session } from 'electron';
import { resolve } from 'path';
import isDev from 'electron-is-dev';
import { REACT_DEVELOPER_TOOLS } from 'electron-devtools-installer';


export class Main {
    app: App;
    mainWindow: BrowserWindow;
    static BrowserWindow: typeof BrowserWindow;

    constructor(app: Electron.App, BW: typeof BrowserWindow) {
        this.app           = app;
        Main.BrowserWindow = BW;
        app.on('ready', this.onReady);

        app.on('window-all-closed', this.onWindowAllClosed);
        app.on('activate', this.onActivate);

        // session.defaultSession.webRequest.onHeadersReceived((details, callback) => {
        //     callback({
        //         responseHeaders: {
        //             ...details.responseHeaders,
        //             'Content-Security-Policy': [ `'*'` ],
        //         },
        //     });
        // });
    }

    createWindow() {
        let url         = isDev ? 'http://localhost:3000/index.html' : `file://${resolve(__dirname, '../public/index.html')}`;
        this.mainWindow = new Main.BrowserWindow({ width: 800, height: 600 });
        this.mainWindow.loadURL(url, {});
        this.mainWindow.on('closed', this.onClose);
    }

    async installExtensions() {

        const installer     = require('electron-devtools-installer');
        const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
        const extensions    = [
            'REACT_DEVELOPER_TOOLS',
            'REDUX_DEVTOOLS',
        ];

        return Promise
            .all(extensions.map(name => installer.default(installer[ name ], forceDownload)))
            .catch(console.log);

    }

    onWindowAllClosed = () => process.platform !== 'darwin' && this.app.quit();
    onReady           = async () => {
        await this.installExtensions();
        this.createWindow();
    };
    onActivate        = () => this.mainWindow === null && this.createWindow();
    onClose           = () => this.mainWindow = null;
}


//
//
// let mainWindow: BrowserWindow;
//
// function createWindow() {
//     mainWindow = new BrowserWindow({ width: 900, height: 680 });
//     mainWindow.loadURL(
//         isDev
//         ? 'http://localhost:3000'
//         : `file://${join(__dirname, '../build/index.html')}`,
//     );
//     mainWindow.on('closed', () => (mainWindow = null));
// }
//
// app.on('ready', createWindow);
// app.on('window-all-closed', () => {
//     if ( process.platform !== 'darwin' ) {
//         app.quit();
//     }
// });
// app.on('activate', () => {
//     if ( mainWindow === null ) {
//         createWindow();
//     }
// });
