"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
require('electron-builder');
var MenuBuilder = /** @class */ (function () {
    function MenuBuilder(mainWindow) {
        this.mainWindow = mainWindow;
    }
    MenuBuilder.prototype.buildMenu = function () {
        if (process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true') {
            this.setupDevelopmentEnvironment();
        }
        var template = process.platform === 'darwin'
            ? this.buildDarwinTemplate()
            : this.buildDefaultTemplate();
        var menu = electron_1.Menu.buildFromTemplate(template);
        electron_1.Menu.setApplicationMenu(menu);
        return menu;
    };
    MenuBuilder.prototype.setupDevelopmentEnvironment = function () {
        var _this = this;
        this.mainWindow.openDevTools();
        this.mainWindow.webContents.on('context-menu', function (e, props) {
            var x = props.x, y = props.y;
            electron_1.Menu
                .buildFromTemplate([{
                    label: 'Inspect element',
                    click: function () {
                        _this.mainWindow.inspectElement(x, y);
                    }
                }])
                .popup(_this.mainWindow);
        });
    };
    MenuBuilder.prototype.buildDarwinTemplate = function () {
        var _this = this;
        var subMenuAbout = {
            label: 'Electron',
            submenu: [
                { label: 'About ElectronReact', selector: 'orderFrontStandardAboutPanel:' },
                { type: 'separator' },
                { label: 'Services', submenu: [] },
                { type: 'separator' },
                { label: 'Hide ElectronReact', accelerator: 'Command+H', selector: 'hide:' },
                { label: 'Hide Others', accelerator: 'Command+Shift+H', selector: 'hideOtherApplications:' },
                { label: 'Show All', selector: 'unhideAllApplications:' },
                { type: 'separator' },
                { label: 'Quit', accelerator: 'Command+Q', click: function () { electron_1.app.quit(); } }
            ]
        };
        var subMenuEdit = {
            label: 'Edit',
            submenu: [
                { label: 'Undo', accelerator: 'Command+Z', selector: 'undo:' },
                { label: 'Redo', accelerator: 'Shift+Command+Z', selector: 'redo:' },
                { type: 'separator' },
                { label: 'Cut', accelerator: 'Command+X', selector: 'cut:' },
                { label: 'Copy', accelerator: 'Command+C', selector: 'copy:' },
                { label: 'Paste', accelerator: 'Command+V', selector: 'paste:' },
                { label: 'Select All', accelerator: 'Command+A', selector: 'selectAll:' }
            ]
        };
        var subMenuViewDev = {
            label: 'View',
            submenu: [
                { label: 'Reload', accelerator: 'Command+R', click: function () { _this.mainWindow.webContents.reload(); } },
                { label: 'Toggle Full Screen', accelerator: 'Ctrl+Command+F', click: function () { _this.mainWindow.setFullScreen(!_this.mainWindow.isFullScreen()); } },
                { label: 'Toggle Developer Tools', accelerator: 'Alt+Command+I', click: function () { _this.mainWindow.toggleDevTools(); } }
            ]
        };
        var subMenuViewProd = {
            label: 'View',
            submenu: [
                { label: 'Toggle Full Screen', accelerator: 'Ctrl+Command+F', click: function () { _this.mainWindow.setFullScreen(!_this.mainWindow.isFullScreen()); } }
            ]
        };
        var subMenuWindow = {
            label: 'Window',
            submenu: [
                { label: 'Minimize', accelerator: 'Command+M', selector: 'performMiniaturize:' },
                { label: 'Close', accelerator: 'Command+W', selector: 'performClose:' },
                { type: 'separator' },
                { label: 'Bring All to Front', selector: 'arrangeInFront:' }
            ]
        };
        var subMenuHelp = {
            label: 'Help',
            submenu: [
                { label: 'Learn More', click: function () { electron_1.shell.openExternal('http://electron.atom.io'); } },
                { label: 'Documentation', click: function () { electron_1.shell.openExternal('https://github.com/atom/electron/tree/master/docs#readme'); } },
                { label: 'Community Discussions', click: function () { electron_1.shell.openExternal('https://discuss.atom.io/c/electron'); } },
                { label: 'Search Issues', click: function () { electron_1.shell.openExternal('https://github.com/atom/electron/issues'); } }
            ]
        };
        var subMenuView = process.env.NODE_ENV === 'development'
            ? subMenuViewDev
            : subMenuViewProd;
        return [
            subMenuAbout,
            subMenuEdit,
            subMenuView,
            subMenuWindow,
            subMenuHelp
        ];
    };
    MenuBuilder.prototype.buildDefaultTemplate = function () {
        var _this = this;
        var templateDefault = [{
                label: '&File',
                submenu: [{
                        label: '&Open',
                        accelerator: 'Ctrl+O'
                    }, {
                        label: '&Close',
                        accelerator: 'Ctrl+W',
                        click: function () {
                            _this.mainWindow.close();
                        }
                    }]
            }, {
                label: '&View',
                submenu: (process.env.NODE_ENV === 'development') ? [{
                        label: '&Reload',
                        accelerator: 'Ctrl+R',
                        click: function () {
                            _this.mainWindow.webContents.reload();
                        }
                    }, {
                        label: 'Toggle &Full Screen',
                        accelerator: 'F11',
                        click: function () {
                            _this.mainWindow.setFullScreen(!_this.mainWindow.isFullScreen());
                        }
                    }, {
                        label: 'Toggle &Developer Tools',
                        accelerator: 'Alt+Ctrl+I',
                        click: function () {
                            _this.mainWindow.toggleDevTools();
                        }
                    }] : [{
                        label: 'Toggle &Full Screen',
                        accelerator: 'F11',
                        click: function () {
                            _this.mainWindow.setFullScreen(!_this.mainWindow.isFullScreen());
                        }
                    }]
            }, {
                label: 'Help',
                submenu: [{
                        label: 'Learn More',
                        click: function () {
                            electron_1.shell.openExternal('http://electron.atom.io');
                        }
                    }, {
                        label: 'Documentation',
                        click: function () {
                            electron_1.shell.openExternal('https://github.com/atom/electron/tree/master/docs#readme');
                        }
                    }, {
                        label: 'Community Discussions',
                        click: function () {
                            electron_1.shell.openExternal('https://discuss.atom.io/c/electron');
                        }
                    }, {
                        label: 'Search Issues',
                        click: function () {
                            electron_1.shell.openExternal('https://github.com/atom/electron/issues');
                        }
                    }]
            }];
        return templateDefault;
    };
    return MenuBuilder;
}());
exports.MenuBuilder = MenuBuilder;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWVudUJ1aWxkZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJNZW51QnVpbGRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBLHFDQUEyRDtBQUMzRCxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtBQUMzQjtJQUdJLHFCQUFZLFVBQXlCO1FBQ2pDLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO0lBQ2pDLENBQUM7SUFFRCwrQkFBUyxHQUFUO1FBQ0ksSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsS0FBSyxhQUFhLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEtBQUssTUFBTSxFQUFFO1lBQzdFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1NBQ3RDO1FBRUQsSUFBTSxRQUFRLEdBQU8sT0FBTyxDQUFDLFFBQVEsS0FBSyxRQUFRO1lBQ2pDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDNUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBRS9DLElBQU0sSUFBSSxHQUFHLGVBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM5QyxlQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFOUIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELGlEQUEyQixHQUEzQjtRQUFBLGlCQWNDO1FBYkcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLFVBQUMsQ0FBQyxFQUFFLEtBQUs7WUFDNUMsSUFBQSxXQUFDLEVBQUUsV0FBQyxDQUFXO1lBRXZCLGVBQUk7aUJBQ0MsaUJBQWlCLENBQUMsQ0FBQztvQkFDaEIsS0FBSyxFQUFFLGlCQUFpQjtvQkFDeEIsS0FBSyxFQUFFO3dCQUNILEtBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDekMsQ0FBQztpQkFDSixDQUFDLENBQUM7aUJBQ0YsS0FBSyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx5Q0FBbUIsR0FBbkI7UUFBQSxpQkF1RUM7UUF0RUcsSUFBTSxZQUFZLEdBQUc7WUFDakIsS0FBSyxFQUFFLFVBQVU7WUFDakIsT0FBTyxFQUFFO2dCQUNMLEVBQUUsS0FBSyxFQUFFLHFCQUFxQixFQUFFLFFBQVEsRUFBRSwrQkFBK0IsRUFBRTtnQkFDM0UsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFO2dCQUNyQixFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRTtnQkFDbEMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFO2dCQUNyQixFQUFFLEtBQUssRUFBRSxvQkFBb0IsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUU7Z0JBQzVFLEVBQUUsS0FBSyxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLHdCQUF3QixFQUFFO2dCQUM1RixFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLHdCQUF3QixFQUFFO2dCQUN6RCxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUU7Z0JBQ3JCLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxjQUFRLGNBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTthQUM1RTtTQUNKLENBQUM7UUFDRixJQUFNLFdBQVcsR0FBRztZQUNoQixLQUFLLEVBQUUsTUFBTTtZQUNiLE9BQU8sRUFBRTtnQkFDTCxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFO2dCQUM5RCxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLGlCQUFpQixFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUU7Z0JBQ3BFLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRTtnQkFDckIsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRTtnQkFDNUQsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRTtnQkFDOUQsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRTtnQkFDaEUsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRTthQUM1RTtTQUNKLENBQUM7UUFDRixJQUFNLGNBQWMsR0FBRztZQUNuQixLQUFLLEVBQUUsTUFBTTtZQUNiLE9BQU8sRUFBRTtnQkFDTCxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsY0FBUSxLQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDckcsRUFBRSxLQUFLLEVBQUUsb0JBQW9CLEVBQUUsV0FBVyxFQUFFLGdCQUFnQixFQUFFLEtBQUssRUFBRSxjQUFRLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNoSixFQUFFLEtBQUssRUFBRSx3QkFBd0IsRUFBRSxXQUFXLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxjQUFRLEtBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7YUFDeEg7U0FDSixDQUFDO1FBQ0YsSUFBTSxlQUFlLEdBQUc7WUFDcEIsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUU7Z0JBQ0wsRUFBRSxLQUFLLEVBQUUsb0JBQW9CLEVBQUUsV0FBVyxFQUFFLGdCQUFnQixFQUFFLEtBQUssRUFBRSxjQUFRLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2FBQ25KO1NBQ0osQ0FBQztRQUNGLElBQU0sYUFBYSxHQUFHO1lBQ2xCLEtBQUssRUFBRSxRQUFRO1lBQ2YsT0FBTyxFQUFFO2dCQUNMLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxxQkFBcUIsRUFBRTtnQkFDaEYsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRTtnQkFDdkUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFO2dCQUNyQixFQUFFLEtBQUssRUFBRSxvQkFBb0IsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUU7YUFDL0Q7U0FDSixDQUFDO1FBQ0YsSUFBTSxXQUFXLEdBQUc7WUFDaEIsS0FBSyxFQUFFLE1BQU07WUFDYixPQUFPLEVBQUU7Z0JBQ0wsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLEtBQUssZ0JBQUssZ0JBQUssQ0FBQyxZQUFZLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDbkYsRUFBRSxLQUFLLEVBQUUsZUFBZSxFQUFFLEtBQUssZ0JBQUssZ0JBQUssQ0FBQyxZQUFZLENBQUMsMERBQTBELENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDdkgsRUFBRSxLQUFLLEVBQUUsdUJBQXVCLEVBQUUsS0FBSyxnQkFBSyxnQkFBSyxDQUFDLFlBQVksQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUN6RyxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUUsS0FBSyxnQkFBSyxnQkFBSyxDQUFDLFlBQVksQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2FBQ3pHO1NBQ0osQ0FBQztRQUVGLElBQU0sV0FBVyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxLQUFLLGFBQWE7WUFDdEMsQ0FBQyxDQUFDLGNBQWM7WUFDaEIsQ0FBQyxDQUFDLGVBQWUsQ0FBQztRQUV0QyxPQUFPO1lBQ0gsWUFBWTtZQUNaLFdBQVc7WUFDWCxXQUFXO1lBQ1gsYUFBYTtZQUNiLFdBQVc7U0FDZCxDQUFDO0lBQ04sQ0FBQztJQUVELDBDQUFvQixHQUFwQjtRQUFBLGlCQWtFQztRQWpFRyxJQUFNLGVBQWUsR0FBRyxDQUFDO2dCQUNyQixLQUFLLEVBQUUsT0FBTztnQkFDZCxPQUFPLEVBQUUsQ0FBQzt3QkFDTixLQUFLLEVBQUUsT0FBTzt3QkFDZCxXQUFXLEVBQUUsUUFBUTtxQkFDeEIsRUFBRTt3QkFDQyxLQUFLLEVBQUUsUUFBUTt3QkFDZixXQUFXLEVBQUUsUUFBUTt3QkFDckIsS0FBSyxFQUFFOzRCQUNILEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBQzVCLENBQUM7cUJBQ0osQ0FBQzthQUNMLEVBQUU7Z0JBQ0MsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEtBQUssYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2pELEtBQUssRUFBRSxTQUFTO3dCQUNoQixXQUFXLEVBQUUsUUFBUTt3QkFDckIsS0FBSyxFQUFFOzRCQUNILEtBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUN6QyxDQUFDO3FCQUNKLEVBQUU7d0JBQ0MsS0FBSyxFQUFFLHFCQUFxQjt3QkFDNUIsV0FBVyxFQUFFLEtBQUs7d0JBQ2xCLEtBQUssRUFBRTs0QkFDSCxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsQ0FBQztxQkFDSixFQUFFO3dCQUNDLEtBQUssRUFBRSx5QkFBeUI7d0JBQ2hDLFdBQVcsRUFBRSxZQUFZO3dCQUN6QixLQUFLLEVBQUU7NEJBQ0gsS0FBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQzt3QkFDckMsQ0FBQztxQkFDSixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ0YsS0FBSyxFQUFFLHFCQUFxQjt3QkFDNUIsV0FBVyxFQUFFLEtBQUs7d0JBQ2xCLEtBQUssRUFBRTs0QkFDSCxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsQ0FBQztxQkFDSixDQUFDO2FBQ0wsRUFBRTtnQkFDQyxLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsQ0FBQzt3QkFDTixLQUFLLEVBQUUsWUFBWTt3QkFDbkIsS0FBSzs0QkFDRCxnQkFBSyxDQUFDLFlBQVksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO3dCQUNsRCxDQUFDO3FCQUNKLEVBQUU7d0JBQ0MsS0FBSyxFQUFFLGVBQWU7d0JBQ3RCLEtBQUs7NEJBQ0QsZ0JBQUssQ0FBQyxZQUFZLENBQUMsMERBQTBELENBQUMsQ0FBQzt3QkFDbkYsQ0FBQztxQkFDSixFQUFFO3dCQUNDLEtBQUssRUFBRSx1QkFBdUI7d0JBQzlCLEtBQUs7NEJBQ0QsZ0JBQUssQ0FBQyxZQUFZLENBQUMsb0NBQW9DLENBQUMsQ0FBQzt3QkFDN0QsQ0FBQztxQkFDSixFQUFFO3dCQUNDLEtBQUssRUFBRSxlQUFlO3dCQUN0QixLQUFLOzRCQUNELGdCQUFLLENBQUMsWUFBWSxDQUFDLHlDQUF5QyxDQUFDLENBQUM7d0JBQ2xFLENBQUM7cUJBQ0osQ0FBQzthQUNMLENBQUMsQ0FBQztRQUVILE9BQU8sZUFBZSxDQUFDO0lBQzNCLENBQUM7SUFDTCxrQkFBQztBQUFELENBQUMsQUFsTEQsSUFrTEM7QUFsTFksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJcbmltcG9ydCB7IGFwcCwgTWVudSwgc2hlbGwsIEJyb3dzZXJXaW5kb3cgfSBmcm9tICdlbGVjdHJvbic7XG5yZXF1aXJlKCdlbGVjdHJvbi1idWlsZGVyJylcbmV4cG9ydCBjbGFzcyBNZW51QnVpbGRlciB7XG4gICAgbWFpbldpbmRvdzogQnJvd3NlcldpbmRvdztcblxuICAgIGNvbnN0cnVjdG9yKG1haW5XaW5kb3c6IEJyb3dzZXJXaW5kb3cpIHtcbiAgICAgICAgdGhpcy5tYWluV2luZG93ID0gbWFpbldpbmRvdztcbiAgICB9XG5cbiAgICBidWlsZE1lbnUoKSB7XG4gICAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ2RldmVsb3BtZW50JyB8fCBwcm9jZXNzLmVudi5ERUJVR19QUk9EID09PSAndHJ1ZScpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0dXBEZXZlbG9wbWVudEVudmlyb25tZW50KCk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCB0ZW1wbGF0ZTphbnkgPSBwcm9jZXNzLnBsYXRmb3JtID09PSAnZGFyd2luJ1xuICAgICAgICAgICAgICAgICAgICAgICAgID8gdGhpcy5idWlsZERhcndpblRlbXBsYXRlKClcbiAgICAgICAgICAgICAgICAgICAgICAgICA6IHRoaXMuYnVpbGREZWZhdWx0VGVtcGxhdGUoKTtcblxuICAgICAgICBjb25zdCBtZW51ID0gTWVudS5idWlsZEZyb21UZW1wbGF0ZSh0ZW1wbGF0ZSk7XG4gICAgICAgIE1lbnUuc2V0QXBwbGljYXRpb25NZW51KG1lbnUpO1xuXG4gICAgICAgIHJldHVybiBtZW51O1xuICAgIH1cblxuICAgIHNldHVwRGV2ZWxvcG1lbnRFbnZpcm9ubWVudCgpIHtcbiAgICAgICAgdGhpcy5tYWluV2luZG93Lm9wZW5EZXZUb29scygpO1xuICAgICAgICB0aGlzLm1haW5XaW5kb3cud2ViQ29udGVudHMub24oJ2NvbnRleHQtbWVudScsIChlLCBwcm9wcykgPT4ge1xuICAgICAgICAgICAgY29uc3QgeyB4LCB5IH0gPSBwcm9wcztcblxuICAgICAgICAgICAgTWVudVxuICAgICAgICAgICAgICAgIC5idWlsZEZyb21UZW1wbGF0ZShbe1xuICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ0luc3BlY3QgZWxlbWVudCcsXG4gICAgICAgICAgICAgICAgICAgIGNsaWNrOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1haW5XaW5kb3cuaW5zcGVjdEVsZW1lbnQoeCwgeSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XSlcbiAgICAgICAgICAgICAgICAucG9wdXAodGhpcy5tYWluV2luZG93KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgYnVpbGREYXJ3aW5UZW1wbGF0ZSgpIHtcbiAgICAgICAgY29uc3Qgc3ViTWVudUFib3V0ID0ge1xuICAgICAgICAgICAgbGFiZWw6ICdFbGVjdHJvbicsXG4gICAgICAgICAgICBzdWJtZW51OiBbXG4gICAgICAgICAgICAgICAgeyBsYWJlbDogJ0Fib3V0IEVsZWN0cm9uUmVhY3QnLCBzZWxlY3RvcjogJ29yZGVyRnJvbnRTdGFuZGFyZEFib3V0UGFuZWw6JyB9LFxuICAgICAgICAgICAgICAgIHsgdHlwZTogJ3NlcGFyYXRvcicgfSxcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnU2VydmljZXMnLCBzdWJtZW51OiBbXSB9LFxuICAgICAgICAgICAgICAgIHsgdHlwZTogJ3NlcGFyYXRvcicgfSxcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnSGlkZSBFbGVjdHJvblJlYWN0JywgYWNjZWxlcmF0b3I6ICdDb21tYW5kK0gnLCBzZWxlY3RvcjogJ2hpZGU6JyB9LFxuICAgICAgICAgICAgICAgIHsgbGFiZWw6ICdIaWRlIE90aGVycycsIGFjY2VsZXJhdG9yOiAnQ29tbWFuZCtTaGlmdCtIJywgc2VsZWN0b3I6ICdoaWRlT3RoZXJBcHBsaWNhdGlvbnM6JyB9LFxuICAgICAgICAgICAgICAgIHsgbGFiZWw6ICdTaG93IEFsbCcsIHNlbGVjdG9yOiAndW5oaWRlQWxsQXBwbGljYXRpb25zOicgfSxcbiAgICAgICAgICAgICAgICB7IHR5cGU6ICdzZXBhcmF0b3InIH0sXG4gICAgICAgICAgICAgICAgeyBsYWJlbDogJ1F1aXQnLCBhY2NlbGVyYXRvcjogJ0NvbW1hbmQrUScsIGNsaWNrOiAoKSA9PiB7IGFwcC5xdWl0KCk7IH0gfVxuICAgICAgICAgICAgXVxuICAgICAgICB9O1xuICAgICAgICBjb25zdCBzdWJNZW51RWRpdCA9IHtcbiAgICAgICAgICAgIGxhYmVsOiAnRWRpdCcsXG4gICAgICAgICAgICBzdWJtZW51OiBbXG4gICAgICAgICAgICAgICAgeyBsYWJlbDogJ1VuZG8nLCBhY2NlbGVyYXRvcjogJ0NvbW1hbmQrWicsIHNlbGVjdG9yOiAndW5kbzonIH0sXG4gICAgICAgICAgICAgICAgeyBsYWJlbDogJ1JlZG8nLCBhY2NlbGVyYXRvcjogJ1NoaWZ0K0NvbW1hbmQrWicsIHNlbGVjdG9yOiAncmVkbzonIH0sXG4gICAgICAgICAgICAgICAgeyB0eXBlOiAnc2VwYXJhdG9yJyB9LFxuICAgICAgICAgICAgICAgIHsgbGFiZWw6ICdDdXQnLCBhY2NlbGVyYXRvcjogJ0NvbW1hbmQrWCcsIHNlbGVjdG9yOiAnY3V0OicgfSxcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnQ29weScsIGFjY2VsZXJhdG9yOiAnQ29tbWFuZCtDJywgc2VsZWN0b3I6ICdjb3B5OicgfSxcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnUGFzdGUnLCBhY2NlbGVyYXRvcjogJ0NvbW1hbmQrVicsIHNlbGVjdG9yOiAncGFzdGU6JyB9LFxuICAgICAgICAgICAgICAgIHsgbGFiZWw6ICdTZWxlY3QgQWxsJywgYWNjZWxlcmF0b3I6ICdDb21tYW5kK0EnLCBzZWxlY3RvcjogJ3NlbGVjdEFsbDonIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICAgICAgY29uc3Qgc3ViTWVudVZpZXdEZXYgPSB7XG4gICAgICAgICAgICBsYWJlbDogJ1ZpZXcnLFxuICAgICAgICAgICAgc3VibWVudTogW1xuICAgICAgICAgICAgICAgIHsgbGFiZWw6ICdSZWxvYWQnLCBhY2NlbGVyYXRvcjogJ0NvbW1hbmQrUicsIGNsaWNrOiAoKSA9PiB7IHRoaXMubWFpbldpbmRvdy53ZWJDb250ZW50cy5yZWxvYWQoKTsgfSB9LFxuICAgICAgICAgICAgICAgIHsgbGFiZWw6ICdUb2dnbGUgRnVsbCBTY3JlZW4nLCBhY2NlbGVyYXRvcjogJ0N0cmwrQ29tbWFuZCtGJywgY2xpY2s6ICgpID0+IHsgdGhpcy5tYWluV2luZG93LnNldEZ1bGxTY3JlZW4oIXRoaXMubWFpbldpbmRvdy5pc0Z1bGxTY3JlZW4oKSk7IH0gfSxcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnVG9nZ2xlIERldmVsb3BlciBUb29scycsIGFjY2VsZXJhdG9yOiAnQWx0K0NvbW1hbmQrSScsIGNsaWNrOiAoKSA9PiB7IHRoaXMubWFpbldpbmRvdy50b2dnbGVEZXZUb29scygpOyB9IH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICAgICAgY29uc3Qgc3ViTWVudVZpZXdQcm9kID0ge1xuICAgICAgICAgICAgbGFiZWw6ICdWaWV3JyxcbiAgICAgICAgICAgIHN1Ym1lbnU6IFtcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnVG9nZ2xlIEZ1bGwgU2NyZWVuJywgYWNjZWxlcmF0b3I6ICdDdHJsK0NvbW1hbmQrRicsIGNsaWNrOiAoKSA9PiB7IHRoaXMubWFpbldpbmRvdy5zZXRGdWxsU2NyZWVuKCF0aGlzLm1haW5XaW5kb3cuaXNGdWxsU2NyZWVuKCkpOyB9IH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICAgICAgY29uc3Qgc3ViTWVudVdpbmRvdyA9IHtcbiAgICAgICAgICAgIGxhYmVsOiAnV2luZG93JyxcbiAgICAgICAgICAgIHN1Ym1lbnU6IFtcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnTWluaW1pemUnLCBhY2NlbGVyYXRvcjogJ0NvbW1hbmQrTScsIHNlbGVjdG9yOiAncGVyZm9ybU1pbmlhdHVyaXplOicgfSxcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnQ2xvc2UnLCBhY2NlbGVyYXRvcjogJ0NvbW1hbmQrVycsIHNlbGVjdG9yOiAncGVyZm9ybUNsb3NlOicgfSxcbiAgICAgICAgICAgICAgICB7IHR5cGU6ICdzZXBhcmF0b3InIH0sXG4gICAgICAgICAgICAgICAgeyBsYWJlbDogJ0JyaW5nIEFsbCB0byBGcm9udCcsIHNlbGVjdG9yOiAnYXJyYW5nZUluRnJvbnQ6JyB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH07XG4gICAgICAgIGNvbnN0IHN1Yk1lbnVIZWxwID0ge1xuICAgICAgICAgICAgbGFiZWw6ICdIZWxwJyxcbiAgICAgICAgICAgIHN1Ym1lbnU6IFtcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnTGVhcm4gTW9yZScsIGNsaWNrKCkgeyBzaGVsbC5vcGVuRXh0ZXJuYWwoJ2h0dHA6Ly9lbGVjdHJvbi5hdG9tLmlvJyk7IH0gfSxcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnRG9jdW1lbnRhdGlvbicsIGNsaWNrKCkgeyBzaGVsbC5vcGVuRXh0ZXJuYWwoJ2h0dHBzOi8vZ2l0aHViLmNvbS9hdG9tL2VsZWN0cm9uL3RyZWUvbWFzdGVyL2RvY3MjcmVhZG1lJyk7IH0gfSxcbiAgICAgICAgICAgICAgICB7IGxhYmVsOiAnQ29tbXVuaXR5IERpc2N1c3Npb25zJywgY2xpY2soKSB7IHNoZWxsLm9wZW5FeHRlcm5hbCgnaHR0cHM6Ly9kaXNjdXNzLmF0b20uaW8vYy9lbGVjdHJvbicpOyB9IH0sXG4gICAgICAgICAgICAgICAgeyBsYWJlbDogJ1NlYXJjaCBJc3N1ZXMnLCBjbGljaygpIHsgc2hlbGwub3BlbkV4dGVybmFsKCdodHRwczovL2dpdGh1Yi5jb20vYXRvbS9lbGVjdHJvbi9pc3N1ZXMnKTsgfSB9XG4gICAgICAgICAgICBdXG4gICAgICAgIH07XG5cbiAgICAgICAgY29uc3Qgc3ViTWVudVZpZXcgPSBwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ2RldmVsb3BtZW50J1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gc3ViTWVudVZpZXdEZXZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IHN1Yk1lbnVWaWV3UHJvZDtcblxuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgc3ViTWVudUFib3V0LFxuICAgICAgICAgICAgc3ViTWVudUVkaXQsXG4gICAgICAgICAgICBzdWJNZW51VmlldyxcbiAgICAgICAgICAgIHN1Yk1lbnVXaW5kb3csXG4gICAgICAgICAgICBzdWJNZW51SGVscFxuICAgICAgICBdO1xuICAgIH1cblxuICAgIGJ1aWxkRGVmYXVsdFRlbXBsYXRlKCkge1xuICAgICAgICBjb25zdCB0ZW1wbGF0ZURlZmF1bHQgPSBbe1xuICAgICAgICAgICAgbGFiZWw6ICcmRmlsZScsXG4gICAgICAgICAgICBzdWJtZW51OiBbe1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnJk9wZW4nLFxuICAgICAgICAgICAgICAgIGFjY2VsZXJhdG9yOiAnQ3RybCtPJ1xuICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnJkNsb3NlJyxcbiAgICAgICAgICAgICAgICBhY2NlbGVyYXRvcjogJ0N0cmwrVycsXG4gICAgICAgICAgICAgICAgY2xpY2s6ICgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tYWluV2luZG93LmNsb3NlKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfV1cbiAgICAgICAgfSwge1xuICAgICAgICAgICAgbGFiZWw6ICcmVmlldycsXG4gICAgICAgICAgICBzdWJtZW51OiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdkZXZlbG9wbWVudCcpID8gW3tcbiAgICAgICAgICAgICAgICBsYWJlbDogJyZSZWxvYWQnLFxuICAgICAgICAgICAgICAgIGFjY2VsZXJhdG9yOiAnQ3RybCtSJyxcbiAgICAgICAgICAgICAgICBjbGljazogKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1haW5XaW5kb3cud2ViQ29udGVudHMucmVsb2FkKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnVG9nZ2xlICZGdWxsIFNjcmVlbicsXG4gICAgICAgICAgICAgICAgYWNjZWxlcmF0b3I6ICdGMTEnLFxuICAgICAgICAgICAgICAgIGNsaWNrOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWFpbldpbmRvdy5zZXRGdWxsU2NyZWVuKCF0aGlzLm1haW5XaW5kb3cuaXNGdWxsU2NyZWVuKCkpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1RvZ2dsZSAmRGV2ZWxvcGVyIFRvb2xzJyxcbiAgICAgICAgICAgICAgICBhY2NlbGVyYXRvcjogJ0FsdCtDdHJsK0knLFxuICAgICAgICAgICAgICAgIGNsaWNrOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWFpbldpbmRvdy50b2dnbGVEZXZUb29scygpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1dIDogW3tcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1RvZ2dsZSAmRnVsbCBTY3JlZW4nLFxuICAgICAgICAgICAgICAgIGFjY2VsZXJhdG9yOiAnRjExJyxcbiAgICAgICAgICAgICAgICBjbGljazogKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1haW5XaW5kb3cuc2V0RnVsbFNjcmVlbighdGhpcy5tYWluV2luZG93LmlzRnVsbFNjcmVlbigpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XVxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBsYWJlbDogJ0hlbHAnLFxuICAgICAgICAgICAgc3VibWVudTogW3tcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0xlYXJuIE1vcmUnLFxuICAgICAgICAgICAgICAgIGNsaWNrKCkge1xuICAgICAgICAgICAgICAgICAgICBzaGVsbC5vcGVuRXh0ZXJuYWwoJ2h0dHA6Ly9lbGVjdHJvbi5hdG9tLmlvJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnRG9jdW1lbnRhdGlvbicsXG4gICAgICAgICAgICAgICAgY2xpY2soKSB7XG4gICAgICAgICAgICAgICAgICAgIHNoZWxsLm9wZW5FeHRlcm5hbCgnaHR0cHM6Ly9naXRodWIuY29tL2F0b20vZWxlY3Ryb24vdHJlZS9tYXN0ZXIvZG9jcyNyZWFkbWUnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdDb21tdW5pdHkgRGlzY3Vzc2lvbnMnLFxuICAgICAgICAgICAgICAgIGNsaWNrKCkge1xuICAgICAgICAgICAgICAgICAgICBzaGVsbC5vcGVuRXh0ZXJuYWwoJ2h0dHBzOi8vZGlzY3Vzcy5hdG9tLmlvL2MvZWxlY3Ryb24nKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTZWFyY2ggSXNzdWVzJyxcbiAgICAgICAgICAgICAgICBjbGljaygpIHtcbiAgICAgICAgICAgICAgICAgICAgc2hlbGwub3BlbkV4dGVybmFsKCdodHRwczovL2dpdGh1Yi5jb20vYXRvbS9lbGVjdHJvbi9pc3N1ZXMnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XVxuICAgICAgICB9XTtcblxuICAgICAgICByZXR1cm4gdGVtcGxhdGVEZWZhdWx0O1xuICAgIH1cbn0iXX0=