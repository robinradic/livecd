#!/usr/bin/env bash

mydir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
fsdir="$mydir/../fs"

cp -f "$mydir/isolinux/splash.png" "$fsdir/disk/isolinux/splash.png"