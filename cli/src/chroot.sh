#!/usr/bin/env bash

start(){
    mount -t proc none /proc
    mount -t sysfs none /sys
    mount -t devpts none /dev/pts
    export HOME=/etc/skel
    export LC_ALL=C

    dbus-uuidgen > /var/lib/dbus/machine-id
    dpkg-divert --local --rename --add /sbin/initctl
    ln -s /bin/true /sbin/initctl
}

end(){
    apt clean
    rm -r /var/cache/apt/archives/*
    rm -rf /tmp/* ~/.bash_history
    rm /var/lib/dbus/machine-id
    rm /sbin/initctl
    dpkg-divert --rename --remove /sbin/initctl

    umount /proc || umount -lf /proc                   #ignore warning in this command
    umount /sys
    umount /dev/pts
}

run-scripts(){
    local scriptFileNames="$*" # "foo.sh bar.sh"
    for scriptName in ${scriptFileNames[@]} ; do
        echo "chroot run script: /root/$scriptName" >> /chroot.log
        bash "/root/$scriptName"
    done
}

run(){
    shift
    start
    run-scripts $*
    end
}

shell(){
    start
    /bin/bash -i
    end
}

echo "chroot.sh ($*)" >> /chroot.log




$*