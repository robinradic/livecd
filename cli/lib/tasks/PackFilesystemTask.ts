import { Task } from '../classes/Task';

export class PackFilesystemTask extends Task {
    constructor() {
        super('pack.filesystem', { title: 'Package Filesystem', description: 'Pack filesystem' });
    }

    public async handle(): Promise<any> {
        let diskPath = this.paths[ 'project.generated.disk' ];
        //regen manifest
        this.exec(`chmod +w ${diskPath}/casper/filesystem.manifest`);
        this.exec(`chroot ${this.paths[ 'project.generated.filesystem' ]} dpkg-query -W --showformat='\${Package} \${Version}\\n' > ${diskPath}/casper/filesystem.manifest`);
        this.exec(`cp -f ${diskPath}/casper/filesystem.manifest ${diskPath}/casper/filesystem.manifest-desktop`);
        this.exec(`sed -i '/ubiquity/d' ${diskPath}/casper/filesystem.manifest-desktop`);
        this.exec(`sed -i '/casper/d' ${diskPath}/casper/filesystem.manifest-desktop`);
        this.exec(`sed -i '/casper/d' ${diskPath}/casper/filesystem.manifest-desktop`);

        // compress
        this.exec(`rm -rf ${diskPath}/casper/filesystem.squashfs`);
        if ( this.app.options.mode === 'development' ) {
            this.exec(`mksquashfs ${this.paths[ 'project.generated.filesystem' ]} ${diskPath}/casper/filesystem.squashfs -noD -comp xz`);
        } else {
            this.exec(`mksquashfs ${this.paths[ 'project.generated.filesystem' ]} ${diskPath}/casper/filesystem.squashfs -comp xz`);
        }

        // update required files
        this.exec(`printf $(du -sx --block-size=1 ${this.paths[ 'project.generated.filesystem' ]} | cut -f1) > ${diskPath}/casper/filesystem.size`);

        // calculate new md5sums
        // this.exec(`cd extracted`)
        this.exec(`rm -rf MD5SUMS`, diskPath);
        this.exec(`find -type f -print0 | xargs -0 md5sum | grep -v isolinux/boot.cat | tee MD5SUMS`, diskPath);
    }
}