import { Task } from '../classes/Task';
import { ensureCopyFileSync, ensureDirSync } from '../utils/fs';

export class InitTask extends Task {
    constructor() {
        super('init', { title: 'Init', description: 'Initialize a new project' });
    }

    public async handle(): Promise<any> {
        ensureDirSync('src');
        ensureCopyFileSync(
            this.paths.get('cli.lib', 'config.json'),
            this.paths.get('project', 'config.json'),
            false,
        );
    }
}