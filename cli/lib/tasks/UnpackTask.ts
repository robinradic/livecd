import { Task } from '../classes/Task';
import { ensureCopyFileSync, ensureDirSync, ensureRemoveSync } from '../utils/fs';
import { existsSync } from 'fs';

export class UnpackTask extends Task {
    constructor() {
        super('unpack', { title: 'Unpack', description: 'Unpack the livecd iso' });
    }

    public async handle(): Promise<any> {
        ensureDirSync(this.paths[ 'project.generated' ]);
        ensureDirSync(this.paths[ 'project.generated.mnt' ]);

        ensureCopyFileSync(this.paths[ 'cli.src.iso' ], this.paths.get('project.tmp', 'tessa.iso'));


        ensureDirSync(this.paths[ 'project.generated.disk' ], true);
        const tmpDisk = this.paths.get('project.tmp', 'generated/disk');
        if ( !existsSync(tmpDisk) ) {
            ensureDirSync(tmpDisk)
            this.exec(`umount --force ${this.paths[ 'project.generated.mnt' ]} &> /dev/null`);
            this.exec(`mount -o loop ${this.paths.get('project.tmp', 'tessa.iso')} ${this.paths[ 'project.generated.mnt' ]}`);
            this.exec(`rsync --exclude=/casper/filesystem.squashfs -a ${this.paths[ 'project.generated.mnt' ]}/ ${tmpDisk}`);
        }
        this.exec(`cp -rf ${tmpDisk} ${this.paths[ 'project.generated' ]}`);


        ensureDirSync(this.paths[ 'project.generated.filesystem' ], true);
        const tmpFs = this.paths.get('project.tmp', 'generated/filesystem');
        if ( !existsSync(tmpFs) ) {
            this.exec(`unsquashfs ${this.paths[ 'project.generated.mnt' ]}/casper/filesystem.squashfs`);
            this.exec(`mv squashfs-root ${tmpFs}`);
            this.exec(`cp /etc/resolv.conf ${tmpFs}/etc/`);
            this.exec(`cp /etc/hosts ${tmpFs}/etc/`);
        }
        this.exec(`cp -rf ${tmpFs} ${this.paths[ 'project.generated' ]}`);


        ensureDirSync(this.paths[ 'project.generated.initrd' ], true);
        const tmpInitrd = this.paths.get('project.tmp', 'generated/initrd');
        if ( !existsSync(tmpInitrd) ) {
            ensureDirSync(tmpInitrd)
            this.exec(`lzma -dc -S .lz ${this.paths[ 'project.generated.disk' ]}/casper/initrd.lz | cpio -imvd --no-absolute-filenames`, tmpInitrd);
        }
        this.exec(`cp -rf ${tmpInitrd} ${this.paths[ 'project.generated' ]}`);

        this.exec(`umount --force ${this.paths[ 'project.generated.mnt' ]}`);
        ensureRemoveSync(this.paths.get('project.tmp', 'tessa.iso'));
    }
}