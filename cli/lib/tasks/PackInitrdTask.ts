import { Task } from '../classes/Task';

export class PackInitrdTask extends Task {
    constructor() {
        super('pack.initrd', { title: 'Package Initrd', description: 'Pack initrd' });
    }

    public async handle(): Promise<any> {
        this.exec(`rm -f ${this.paths[ 'project.generated.disk' ]}/casper/initrd.lz`);

        if ( this.app.options.mode === 'development' ) {
            this.exec(`find . | cpio --quiet --dereference -o -H newc | lzma -0 > ${this.paths[ 'project.generated.disk' ]}/casper/initrd.lz`, this.paths[ 'project.generated.initrd' ]);
        } else {
            this.exec(`find . | cpio --quiet --dereference -o -H newc | lzma -7 > ${this.paths[ 'project.generated.disk' ]}/casper/initrd.lz`, this.paths[ 'project.generated.initrd' ]);
        }
    }
}