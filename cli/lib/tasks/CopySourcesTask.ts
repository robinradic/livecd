import { Task } from '../classes/Task';

export class CopySourcesTask extends Task {
    constructor() {
        super('copy', { title: 'Copy/Merge', description: 'Copy/Merge Sources and Templates' });
    }

    public async handle(): Promise<any> {
        this.copySources('disk');
        this.copySources('filesystem');
        this.copySources('initrd');
    }
}