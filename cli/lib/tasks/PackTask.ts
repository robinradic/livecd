import { Task } from '../classes/Task';

export class PackTask extends Task {
    constructor() {
        super('pack', {
            title      : 'Pack',
            description: 'Pack',
            depends    : [
                'copy',
                'chroot',
                'pack.filesystem',
                'pack.initrd',
                'pack.iso',
            ],
        });
    }

    public async handle(): Promise<any> {
        console.log('PackTask');
    }
}