import { Task } from '../classes/Task';

export class PackISOTask extends Task {
    constructor() {
        super('pack.iso', { title: 'Package ISO', description: 'Package ISO' });
    }

    public async handle(): Promise<any> {
        if ( this.app.options.mode === 'development' ) {
            this.exec(`cd ${this.paths[ 'project.generated.disk']} && mkisofs  -D -r -V "$IMAGE_NAME" -cache-inodes -allow-limited-size -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ${this.paths[ 'project.generated.iso']} .`, this.paths[ 'project.generated.disk']);
        } else {
            this.exec(`cd ${this.paths[ 'project.generated.disk']} && mkisofs  -D -r -V "$IMAGE_NAME" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ${this.paths[ 'project.generated.iso']} .`, this.paths[ 'project.generated.disk']);
        }
        this.permissions('radic:radic', this.paths[ 'project.generated.iso']);
    }
}