import { InitTask } from './tasks/InitTask';
import { UnpackTask } from './tasks/UnpackTask';
import { PackTask } from './tasks/PackTask';
import { CopySourcesTask } from './tasks/CopySourcesTask';
import { PackFilesystemTask } from './tasks/PackFilesystemTask';
import { PackInitrdTask } from './tasks/PackInitrdTask';
import { PackISOTask } from './tasks/PackISOTask';
import { Application } from './classes/Application';
import { isAbsolute } from 'path';
import { createHash } from 'crypto';
import { copyFileSync, unlinkSync } from 'fs';
import { exec } from './utils/exec';
import { ensureDirSync } from './utils/fs';


export function addTasks(app: Application) {
    app.executor.addTask(app.resolve(InitTask), { priority: 10 });
    app.executor.addTask(app.resolve(UnpackTask), { priority: 100 });
    app.executor.addTask(app.resolve(PackTask), { priority: 150 });
    app.executor.addTask(app.resolve(CopySourcesTask), { priority: 200 });
    app.executor.addTask(app.resolve(PackFilesystemTask), { priority: 400 });
    app.executor.addTask(app.resolve(PackInitrdTask), { priority: 500 });
    app.executor.addTask(app.resolve(PackISOTask), { priority: 600 });

    addChrootTasks(app);
}


export function addChrootTasks(app: Application) {
    let targets                                                                                        = [ 'filesystem' ]; //, 'initrd'
    let scriptFiles: Record<string, Array<{ sourcePath: string, fileName: string, destPath: string }>> = {};

    targets.forEach(target => {
        scriptFiles[ target ] = [];
        if ( Array.isArray(app.config.chroot[ target ]) ) {
            app.config.chroot[ target ].forEach(sourcePath => {
                sourcePath   = isAbsolute(sourcePath) ? sourcePath : app.paths.get('cli.src', sourcePath);
                let fileName = 'chroot_' + createHash('sha256').update(sourcePath).digest('hex').toString() + '.sh';
                app.log.verbose('add script to run in chroot', sourcePath, fileName);
                const destPath = app.paths.get(`project.generated.${target}` as any, 'root', fileName);
                scriptFiles[ target ].push({ sourcePath, fileName, destPath });
            });
        }
    });
    app.executor.addTask('chroot.init', { priority: 310 }, async () => {
        targets.forEach(target => {
            app.mfs.copyFile('src', `chroot.sh`, app.paths.get(`project.generated.${target}` as any, 'chroot.sh'));
            ensureDirSync(app.paths.get(`project.generated.${target}` as any, 'root'));
            scriptFiles[ target ].forEach(scriptFile => {
                copyFileSync(scriptFile.sourcePath, scriptFile.destPath);
            });
        });
    });
    app.executor.addTask('chroot.run', { priority: 320 }, async () => {
        exec(`chroot ${app.paths[ 'project.generated.filesystem' ]} ./chroot.sh run "${scriptFiles.filesystem.map(scriptFile => scriptFile.fileName).join(' ')}"`);
    });
    app.executor.addTask('chroot.clean', { priority: 330 }, async () => {
        targets.forEach(target => {
            scriptFiles[ target ].forEach(scriptFile => unlinkSync(app.paths.get(`project.generated.${target}` as any, 'root', scriptFile.fileName)));
        });
    });
    app.executor.addTask('chroot', { depends: [ 'chroot.init', 'chroot.run', 'chroot.clean' ], priority: 300 }, async () => {
    });
}