import { Container, decorate, inject, injectable, unmanaged } from 'inversify';
import getDecorators from 'inversify-inject-decorators';

export const container = new Container();

export const { lazyInject } = getDecorators(container);

export { injectable, decorate, unmanaged, inject };
