import isRoot from 'is-root';
import { Application } from '../classes/Application';
import { execSync } from 'child_process';
import { logger } from '../utils/logger';

if ( !isRoot() && !process.env.NODE_DEBUG ) {
    let verbose = ['--verbose','-v','-vv','-vvv'].filter(arg => process.argv.includes(arg)).length > 0
    if(verbose){
        logger('Switched to root user');
    }
    execSync(`sudo ${process.argv.join(' ')}`, {
        encoding: 'utf8',
        stdio   : 'inherit',
    });
    process.exit();
}

const app = new Application();

app
    .init({
        argv: process.argv.slice(2),
        cwd : process.cwd(),
    })
    .then(app.run)
    .then(result => {
        process.exitCode = result.exitCode;
    })
    .catch(error => {
        app.log.error(error.message ? error.message.toString() : error.toString());
        if ( error instanceof Error ) {
            app.log.debug(error.stack.toString());
        }
        process.exitCode = 1;
    });
//
// program
//     .description('Create LiveCD')
//     .version(pkg.version, '--version')
//     .option('-c, --config [path]', 'config json/js/ts path')
//
//     .option('-l, --list-tasks', 'List all task names')
//     .option('-r, --run <tasks>', 'Run specific tasks (separated by comma)', (val) => val.split(','), [])
//
//     .option('-p, --production', 'Enables optimal production compression levels (increases build-time substantially)')
//     .option('-m, --more', 'Show more (sub) task choices')
//     .option('-v, --verbose', 'Increase log output verbosity');
//
//
// program.parse(process.argv);
//
// const app2 = new Application({
//     configFile    : program.config,
//     cwd           : process.cwd(),
//     showExecOutput: program.verbose,
//     mode          : program.production ? 'production' : 'dev',
//     expand        : program.more,
// });
//
//
// if ( program.listTasks ) {
//     app.executor.getTasks().forEach(task => {
//         console.log(` - ${chalk.bold(task.name)}   ${chalk.grey('(' + (task.options.description || task.options.title) + ')')}`);
//     });
// } else {
//     app
//         .run(program.run)
//         .then(value => {
//             app.log('Done');
//         })
//         .catch(reason => {
//             app.log.error(reason);
//         });
// }
