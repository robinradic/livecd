import { Command } from './command';
import { logger } from '../../utils/logger';
import { inject } from 'inversify';
import { Parser} from '../../classes/Parser';
import { globalArguments } from '../../constants';
import { Section } from '../../interfaces';


export class HelpCommand implements Command {
    name    = 'help';
    aliases = [];

    description = 'Shows this help message, or help for a specific command';

    args = [ {
        name         : 'command',
        description  : 'The command to display help for',
        defaultOption: true,
    } ];

    commands: Map<String, Command> = new Map();

    @inject('parser') parser: Parser;

    setCommands(commands: Map<String, Command>) {
        this.commands = commands;
        return this;
    }

    generateGeneralUsage() {
        return this.parser.usage([
            {
                content: 'HEADER',
                raw    : true,
            },
            {
                content: `Run 'livecd help <command>' for help with a specific command.`,
                raw    : true,
            },
            {
                header : 'Available Commands',
                content: Array.from(new Set(this.commands.values())).map((command) => {
                    return { name: command.name, summary: command.description };
                }),
            },
            { header: 'Global Options', optionList: globalArguments },
        ]);
    }

    async generateCommandUsage(command: Command) {
        const extraUsageGroups       = command.extraUsageGroups ? await command.extraUsageGroups() : [];
        const usageGroups: Section[] = [
            { header: `livecd ${command.name}`, content: command.description },
            { header: 'Command Options', optionList: command.args },
            { header: 'Global Options', optionList: globalArguments },
        ];

        if ( command.aliases.length > 0 ) {
            usageGroups.splice(1, 0, { header: 'Alias(es)', content: command.aliases });
        }

        return this.parser.usage(usageGroups.concat(extraUsageGroups));
    }

    async run(options?: { command?: string }) {
        const commandName: string = options.command
        if ( !commandName ) {
            logger.debug('no command given, printing general help...', { options: options });
            console.log(this.generateGeneralUsage());
            return;
        }

        const command = this.commands.get(commandName);
        if ( !command ) {
            logger.error(`'${commandName}' is not an available command.`);
            console.log(this.generateGeneralUsage());
            return;
        }

        logger.debug(`printing help for command '${commandName}'...`);
        console.log(await this.generateCommandUsage(command));
    }
}