import { Command } from './command';
import { inject } from 'inversify';
import { Application } from '../../classes/Application';
import { ArgDefinition } from '../../interfaces';


export class VersionCommand implements Command {
    name                  = 'version';
    description           = 'shows the application version';
    aliases               = [];
    args: ArgDefinition[] = [
        { name: 'major', type: Boolean },
        { name: 'minor', type: Boolean },
        { name: 'patch', type: Boolean },
    ];

    @inject('app') app: Application;

    async run() {
        // let steps = 100;
        // const bar = this.app.progress({ barsize: steps });
        // bar.start(steps, 1);
        // let id = setInterval(() => {
        //     if ( steps === 0 ) {
        //         bar.stop();
        //         return clearInterval(id);
        //     }
        //     bar.update(bar.getTotal() - steps);
        //     steps --;
        // }, 10);

        console.log(this.app.pkg.version);
    }
}