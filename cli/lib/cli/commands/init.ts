import { Command } from './command';


export class InitCommand implements Command {
    name        = 'init';
    description = 'initializes a new project in the current directory';
    aliases     = [];
    args        = [
        { name: 'fast', alias: 'f', type: String },
    ];

    async run(commandOptions) {
        console.log('InitCommand run', commandOptions);
    }
}