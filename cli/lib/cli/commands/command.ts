import { OptionDefinition } from 'command-line-args';
import { Section } from 'command-line-usage';

export type CommandOptions = {
    [ name: string ]: any
};

export interface Command {
    name: string;
    aliases: string[];
    description: string;
    args: OptionDefinition[];

    run(...params): Promise<CommandResult | void>

    // run(options: CommandOptions, config: ProjectConfig): Promise<CommandResult|void>;

    /**
     * Documentation to append onto the output of `polymer help commandName`.
     */
    extraUsageGroups?(): Promise<Section[]>;
}

export interface CommandConstructor {
    prototype:Partial<Command>
    new(...params:any[]):Command
}


/**
 * A command may return a CommandResult to indicate an exit code.
 */
export class CommandResult {
    constructor(public exitCode: number) {
    }
}