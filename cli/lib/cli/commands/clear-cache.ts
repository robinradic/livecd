import { Command } from './command';
import { inject } from 'inversify';
import { Application } from '../../classes/Application';
import { ArgDefinition } from '../../interfaces';
import { exec } from '../../utils/exec';


export class ClearCacheCommand implements Command {
    name                  = 'clear-cache';
    description           = 'removes all cache files';
    aliases               = [];
    args: ArgDefinition[] = [];

    @inject('app') app: Application;

    async run() {
        exec(`rm -rf ${this.app.paths.get('cli.tmp')}`);
        exec(`rm -rf ${this.app.paths.get('project.tmp')}`);
        this.app.log.info('clear-cache','cache cleared');
    }
}