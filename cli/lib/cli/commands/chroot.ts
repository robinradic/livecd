import { Command } from './command';
import { inject } from 'inversify';
import { Application } from '../../classes/Application';
import { ArgDefinition } from '../../interfaces';
import { exec } from '../../utils/exec';
import { MirrorFS } from '../../classes/MirrorFS';


export class ChrootCommand implements Command {
    name                  = 'chroot';
    description           = 'chroot into the filesystem';
    aliases               = [];
    args: ArgDefinition[] = [
        { name: 'target', type: String, defaultOption: true, defaultValue: 'filesystem', validate: value => [ 'filesystem', 'initrd' ].includes(value) },
    ];

    @inject('app') app: Application;
    @inject('mfs') mfs: MirrorFS;

    async run(args) {
        await this.app.execute([ 'chroot.init' ]);
        exec(`chroot ${this.app.paths.get(`project.generated.${args.target}` as any)} ./chroot.sh shell`, {
            stdio: 'inherit',
        });
        await this.app.execute([ 'chroot.clean' ]);
    }
}