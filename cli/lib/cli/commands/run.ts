import { Command } from './command';
import { ArgDefinition } from '../../interfaces';
import { inject } from 'inversify';
import { Application } from '../../classes/Application';
import { Logger } from '../../utils/logger';
import { MultiSelect } from 'enquirer';
import chalk from 'chalk';
import { get, set } from 'lodash';


export class RunCommand implements Command {
    name                  = 'run';
    description           = 'Run one or more tasks';
    aliases               = [];
    args: ArgDefinition[] = [
        { name: 'tasks', type: String, defaultOption: true, multiple: true, defaultValue: [], description: 'the names of tasks to run' },
        { name: 'list', alias: 'l', type: Boolean, description: 'print a list of all task names' },
        { name: 'tree', alias: 't', type: Boolean, description: 'print list as tree' },
    ];
    @inject('app') app: Application;
    @inject('log') log: Logger;

    get globals() { return this.app.globals; }

    async run(commandOptions) {
        if ( commandOptions.list ) {
            return this.runList(commandOptions.tree);
        }
        return this.runTasks(commandOptions.tasks);
    }

    protected mapTasksToChoices(names: string[]) {
        return this.app.executor.selectTasks(...names).sorted().map(task => {
            let value: any = task.options.name,
                choices;
            if ( task.depends && task.depends.length ) {
                choices = value = this.mapTasksToChoices(task.depends.map(task => task.toString()));
            }
            return {
                name: task.options.title,
                hint: chalk.grey(task.options.description),
                value,
                choices,
            };
        });
    }

    async runList(asTree = false) {
        if ( asTree ) {
            let tasks     = this.app.executor.getTasks().sorted().map(task => task);
            let tree: any = {};
            tasks.forEach(task => {
                let value = get(tree, task.name, task.depends.length > 0 ? {} : chalk.green(task.name));
                set(tree, task.name, value);
            });
            this.log(require('treeify').asTree(tree, true));
        } else {
            let names = this.app.executor.getTasks().sorted().map(task => task.name);
            let list  = names.map(name => ` - ${name}`).join('\n');
            this.log(list);
        }
    }

    async runTasks(names: string[]) {
        if ( names.length === 0 && this.globals.noInteraction !== true ) {
            const choices = this.mapTasksToChoices([
                'init',
                'unpack',
                'pack',
            ]);
            const prompt  = new MultiSelect({
                type   : 'multiselect',
                name   : 'tasks',
                message: 'Pick tasks to run',
                choices,
                async result(this: MultiSelect, names: any) {
                    const tasks = this.map(names) as Record<string, any>;
                    return Object.values(tasks).filter(val => typeof val === 'string') as any;
                },
            });

            names = await prompt.run();
        }
        if ( names.length === 0 ) {
            throw new Error(`No tasks specified`);
        }
        this.log.debug('tasks', names);
        await this.app.execute(names);
    }
}