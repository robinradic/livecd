import commandLineUsage from 'command-line-usage';
import { inject, injectable } from 'inversify';
import commandLineArgs, { ParseOptions as BaseParseOptions } from 'command-line-args';
import commandLineCommands from 'command-line-commands';
import { ArgDefinition, Content, OptionList, Section } from '../interfaces';
import { cloneDeep } from 'lodash';
import { Application } from './Application';
import { container } from '../container';

export type ParsedArgs<T extends any = any> = {
    _unknown?: string[];
    [ propName: string ]: any;
} & T

export interface ParsedCommands {
    command: string | null,
    argv: string[]
}

export interface ParserOptions extends BaseParseOptions {
    ignoreGroups?: boolean
    skipValidation?: boolean
    skipTransformation?: boolean
    skipHandlers?: boolean
}

const isContent    = (section: any): section is Content => section && (section.content !== undefined || section.raw !== undefined);
const isOptionList = (section: any): section is OptionList => !isContent(section);

@injectable()
export class Parser {
    @inject('app') app: Application;

    options: ParserOptions = {
        camelCase: true,
    };

    protected transformDefinitions(defs: ArgDefinition[]): ArgDefinition[] {
        return cloneDeep(defs).map(def => this.transformDefinition(def));
    }

    protected transformDefinition(def: ArgDefinition): ArgDefinition {
        if ( def.type === Boolean && def.defaultValue === undefined ) {
            def.defaultValue = false;
        }
        if ( this.options.ignoreGroups ) {
            def.group = undefined;
        }
        return def;
    }

    args<T>(definitions: ArgDefinition[], argv: string[], optionsOverride: Partial<ParserOptions> = {}): ParsedArgs<T> {
        let originalOptions = this.options;
        this.options        = {
            argv,
            ...this.options,
            ...optionsOverride,
        };
        definitions         = this.transformDefinitions(definitions);
        const parsed        = commandLineArgs(definitions, this.options) as ParsedArgs<T>;
        for ( const def of definitions ) {
            if ( this.options.skipValidation !== true && typeof def.validate === 'function' ) {
                let valid = def.validate(parsed[ def.name ]);
                if ( valid === false ) {
                    throw new Error(`Invalid value for [${def.name}]`);
                } else if ( typeof valid === 'string' ) {
                    throw new Error(valid);
                }
            }

            if ( this.options.skipTransformation !== true && typeof def.transform === 'function' ) {
                parsed[ def.name ] = def.transform(parsed[ def.name ], container.get('app'));
            }
            if ( this.options.skipHandlers !== true && typeof def.handle === 'function' ) {
                def.handle(parsed[ def.name ], container.get('app'));
            }
        }
        this.options = originalOptions;
        return parsed;
    }

    usage<T>(sections: Section | Section[], optionsOverride: Partial<ParserOptions> = {}): string {
        let originalOptions = this.options;
        this.options        = {
            ...this.options,
            ...optionsOverride,
        };
        sections            = Array.isArray(sections) ? sections : [ sections ];
        sections.forEach((section: Section) => {
            if ( isOptionList(section) && section.optionList !== undefined ) {
                this.transformDefinitions(section.optionList);
            }
        });
        const usage  = commandLineUsage(sections);
        this.options = originalOptions;
        return usage;
    }

    commands(commands: string[], argv: string[]): ParsedCommands {
        return commandLineCommands(commands, argv);
    }

    mergeArguments(argumentLists: ArgDefinition[][]): ArgDefinition[] {
        const argsByName = new Map<string, ArgDefinition>();
        for ( const args of argumentLists ) {
            for ( const arg of args ) {
                argsByName.set(arg.name, Object.assign({}, argsByName.get(arg.name), arg));
            }
        }
        return Array.from(argsByName.values());
    }
}