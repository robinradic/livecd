import { dirname, join, relative, resolve } from 'path';
import { copyFileSync, existsSync } from 'fs';
import { ensureDirSync, listFilesSync } from '../utils/fs';
import { injectable } from '../container';
import { exec } from '../utils/exec';


@injectable()
export class MirrorFS {

    protected mirrors: Record<string, { name: string, path: string, overridePaths: string[] }> = {};

    protected get exec() {return exec }

    protected rsyncOptions: string[] = [ // `--archive`, // --archive = recursive, links, perms, times, group, owner, devices, specials, no-hard-links, no-acls, no-xattrs
        `recursive`, `links`,
        `perms`, `chown=$USER:$USER`, // root:root
        `times`, `devices`, `specials`,
        `no-hard-links`, `no-acls`, `no-xattrs`,
        `verbose`,
    ];

    defineMirror(name: string, path: string, ...overridePaths: string[]) {
        if ( this.mirrors[ name ] === undefined ) {
            this.mirrors[ name ] = { name, path, overridePaths: [] } as any;
        }
        this.mirrors[ name ].path = path;
        this.mirrors[ name ].overridePaths.push(...overridePaths);
        return this;
    }

    copyFile(name: string, mirrorPath: string, destPath: string, override: boolean = false) {
        ensureDirSync(dirname(destPath));
        let filePath = this.resolvePath(name, mirrorPath);
        if ( filePath !== undefined ) {
            copyFileSync(filePath, destPath);
            return true;
        }
        return false;
    }

    rsync(name: string, mirrorPath: string, destPath: string, clear: boolean = false) {
        let { path, overridePaths } = this.mirrors[ name ];
        let paths                   = [ path ].concat(overridePaths);
        ensureDirSync(destPath, clear);
        for ( let sourcePath of paths ) {
            sourcePath = join(sourcePath, mirrorPath);
            if ( !existsSync(sourcePath) ) {
                continue;
            }
            let options = this.rsyncOptions.map(v => `--${v}`).join(' ');
            this.exec(`rsync ${options} ${sourcePath} ${destPath}`);
        }
        return this;
    }

    resolveDir(name: string, mirrorPath: string, recursive: boolean = false): Record<string, string> {
        let { path, overridePaths } = this.mirrors[ name ];
        let paths                   = [ path ].concat(overridePaths);
        let resolved                = {};
        for ( let sourceDirPath of paths ) {
            let fullPath = resolve(sourceDirPath, mirrorPath);
            if ( existsSync(fullPath) ) {
                for ( let filePath of listFilesSync(fullPath, recursive) ) {
                    let relativeFilePath         = relative(sourceDirPath, filePath);
                    resolved[ relativeFilePath ] = filePath; //{ relativePath, filePath }; // resolved[ relativeFilePath ] = {sourceDirPath, relativeFilePath, filePath };
                }
            }
        }
        return resolved;
    }

    resolvePath(name: string, mirrorPath: string) {
        let { path, overridePaths } = this.mirrors[ name ];
        let paths                   = [ path ].concat(overridePaths).reverse();
        for ( let sourcePath of paths ) {
            sourcePath = resolve(sourcePath, mirrorPath);
            if ( existsSync(sourcePath) ) {
                return sourcePath;
            }
        }
    }
}