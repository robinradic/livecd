import * as _winston from 'winston';
import { Logger } from 'winston';
import { TransformableInfo } from 'logform';
import { inspect } from 'util';
import { LEVEL, MESSAGE, SPLAT } from 'triple-beam';
import timestamp from 'time-stamp';
import chalk from 'chalk';
import { cloneDeep } from 'lodash';

export namespace Log {
    export type Level = 'error' | 'warn' | 'info' | 'verbose' | 'debug' | 'silly';
}

export class Log {
    constructor(public readonly winston: Logger) {
        this.warn    = this._log.bind(this, 'warn');
        this.info    = this._log.bind(this, 'info');
        this.verbose = this._log.bind(this, 'verbose');
        this.debug   = this._log.bind(this, 'debug');
        this.silly   = this._log.bind(this, 'silly');
    }


    error(error: Error)
    error(...args: any[])
    error(...args: any[]) {
        if ( args.length > 0 && args[ 0 ] instanceof Error ) {
            this._log('error', args[ 0 ].toString());
            if ( args[ 0 ].stack ) {
                console.log(require('clean-stack')(args[ 0 ].stack));
            }
        } else {
            this._log('error', ...args);
        }
        return this;
    };

    warn(...args: any[]) {return this._log('warn', ...args); }

    info(...args: any[]) {return this._log('info', ...args); }

    verbose(...args: any[]) {return this._log('verbose', ...args); }

    debug(...args: any[]) {return this._log('debug', ...args); }

    silly(...args: any[]) {return this._log('silly', ...args); }

    set level(level: Log.Level) {
        this.winston.level = level;
        this.winston.transports.forEach(transport => transport.level = level);
    }

    get level(): Log.Level {
        return this.winston.level as Log.Level;
    }

    private _log(_level: Log.Level, ...args) {
        this.winston.log.apply(this.winston, arguments);
        return this;
    }

    static createDefault(): Log {
        return createDefaultLog(_winston);
    }
}


export function createDefaultLog(winston: typeof _winston) {
    const meta     = (info, opts: any) => {
        if ( !info[ SPLAT ] ) return '';
        return cloneDeep(info[ SPLAT ]).map(splat => {
            delete splat.label;
            delete splat.labelColor;
            if ( typeof splat === 'object' || Array.isArray(splat) ) {
                if ( Object.keys(splat).length === 0 ) return '';
                return inspect(splat, false, opts.depth || null, opts.colorize);
            }
            return ''
        }).join(' ');
    };
    const prettify = winston.format((info: TransformableInfo, opts: any) => {
        if ( info[ SPLAT ] ) {
            // for ( const splat of info[ SPLAT ] ) {
            //     const inspected = inspect(splat, false, opts.depth || null, opts.colorize);
            //     if ( typeof splat === 'object' || Array.isArray(splat) ) {
            //         info.message += '\n';
            //     }
            //     info.message += inspected;
            // }
            info.message += meta(info, opts);
        }
        if ( info.label ) {
            let labelColors = [ 'cyan' ];
            if ( info.labelColor !== undefined ) {
                labelColors = Array.isArray(info.labelColor) ? info.labelColor : [ info.labelColor ];
            }
            let labelColor = [ chalk ].concat(labelColors as any[]).reduce((previousValue, currentValue) => previousValue[ currentValue.toString() ]) as any;
            info.message   = `${labelColor(info.label)}: ${info.message}`;
        }
        let time  = timestamp('HH:mm:ss');
        let level = info[ LEVEL ].substr(0, 1).toUpperCase();
        if ( opts.colorize ) {
            time           = chalk.grey(time);
            let colors     = _winston.config.npm.colors[ info[ LEVEL ] ];
            colors         = Array.isArray(colors) ? colors : [ colors ];
            let applyColor = [ chalk, 'bold' ].concat(colors as any[]).reduce((previousValue, currentValue) => previousValue[ currentValue.toString() ]) as any;
            level          = applyColor(level);
        }
        info[ MESSAGE ] = `[${time}][${level}] ${info.message}`;
        return info;
    });

    const consoleTransport                           = new winston.transports.Console({
        format: winston.format.combine(
            // winston.format.label({message: false, label: null}),
            winston.format.errors({ stack: true }),
            winston.format.splat(),
            prettify({ colorize: true }),
            // winston.format.simple(),

            // winston.format.timestamp({
            //     format: 'YYYY-MM-DD HH:mm:ss',
            // }),
            // winston.format.errors({ stack: true }),
            // winston.format.splat(),
            // pretty({ colorize: true }),
        ),
    });
    const levelNames: Log.Level[]                    = [ 'error', 'warn', 'info', 'verbose', 'debug', 'silly' ];
    const levels: Partial<Record<Log.Level, number>> = {};
    levelNames.forEach((levelName, level) => levels[ levelName ] = level);


    const logger = winston.createLogger({
        level     : 'info',
        transports: [
            consoleTransport,
        ],
        levels,

    });

    const log = new Log(logger);

    return log;
}