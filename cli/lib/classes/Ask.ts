import * as inquirer from 'inquirer';
import { ChoiceType, Question } from 'inquirer';
/*

         *      <li>input</li>
         *      <li>number</li>
         *      <li>confirm</li>
         *      <li>list</li>
         *      <li>rawlist</li>
         *      <li>password</li>
 */

export type QuestionType =
    | 'input'
    | 'number'
    | 'confirm'
    | 'list'
    | 'rawlist'
    | 'password'
| 'checkbox'
export type QuestionChoices =
    | ReadonlyArray<ChoiceType>
    | ((answers: any) => ReadonlyArray<ChoiceType>)
    | ((answers: any) => Promise<ReadonlyArray<ChoiceType>>);


export class Ask {
    static async prompt<T = any>(type: QuestionType, message: string, options?: Question): Promise<T> {
        let answers = await inquirer.prompt<{ answer: T }>([
            { name: 'answer', message, type, ...options },
        ]);
        return answers.answer;
    }

    static async question(message: string, def?: any, options?: Question) {
        return this.prompt<string>('input', message, {
            default: def,
            ...options,
        });
    }

    static async confirm(message: string, def: any = false, options?: Question) {
        return this.prompt<boolean>('confirm', message, {
            default: def,
            ...options,
        });
    }

    static async list<T = string>(message: string, choices: any[], def?: any, options?: Question) {
        return this.prompt<T>('list', message, {
            choices,
            default: def,
            ...options,
        });
    }

    static async rawlist<T = string[]>(message: string, choices: any[], def?: any, options?: Question) {
        return this.prompt<T>('rawlist', message, {
            choices,
            default: def,
            ...options,
        });
    }
    static async checkbox<T = string>(message: string, choices: any[], def?: any, options?: Question) {
        return this.prompt<T>('checkbox', message, {
            choices,
            default: def,
            ...options,
        });
    }


}