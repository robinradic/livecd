import { Application } from './Application';
import { Config as ConfigUtil } from '../utils/config';
import { Paths } from '../interfaces';
import { Templates } from './Templates';
import { existsSync } from 'fs';
import { dirname, resolve } from 'path';
import { Config } from './Config';
import { ensureDirSync } from '../utils/fs';
import { Executor } from './Executor';
import { DependencySorter } from '@radic/util';
import { firstBy } from '../utils/firstBy';
import { MirrorFS } from './MirrorFS';
import { Logger } from '../utils/logger';
import { exec, sudo } from '../utils/exec';
import { injectable, unmanaged } from 'inversify';

@injectable()
export class Task {
    protected running: boolean   = false;
    protected completed: boolean = false;
    protected app: Application;
    protected executor: Executor;
    public options: TaskOptions;

    get priority(): number {return this.options.priority;}

    get depends(): Array<string | Task> {return this.options.depends;}

    protected get config(): ConfigUtil {return this.app.config;}

    protected get paths(): Paths {return this.app.paths;}

    protected get templates(): Templates {return this.app.templates;}

    protected get log(): Logger {return this.app.log; }

    protected get exec() {return exec;}

    protected get sudo() {return sudo;}

    protected get mfs(): MirrorFS {return this.app.mfs;}

    constructor(
        @unmanaged() public name: string,
        @unmanaged() options: Partial<TaskOptions> = {},
        @unmanaged() protected handler?: Function,
    ) {
        this.options = createTaskOptions(name, options);
    }

    public init(app: Application, executor: Executor) {
        this.app      = app;
        this.executor = executor;
        return this;
    }

    public async handle() {}

    public async run(force: boolean = false) {
        if ( !force && this.completed ) {
            return;
        }
        this.completed = false;
        this.running   = true;
        let res;
        if ( typeof this.handler === 'function' ) {
            this.handler.bind(this);
            res = await this.handler();
        } else {
            res = await this.handle();
        }
        this.completed = true;
        this.running   = false;
        return res;
    }

    public toString() {
        return this.name;
    }

    protected permissions(owner: string, ...paths: string[]) {
        for ( const path of paths ) {
            if ( existsSync(path) ) {
                this.exec(`sudo chown -R ${owner} ${path}`);
                this.exec(`sudo chmod -R 0777 ${path}`);
            }
        }
    }

    protected srcPath(...parts: string[]) { return resolve(__dirname, '..', ...parts); }

    protected path(...parts: string[]) { return resolve(this.paths.cwd, ...parts); }


    protected rsync(src: string, dest: string) {
        let options = [ // `--archive`, // --archive = recursive, links, perms, times, group, owner, devices, specials, no-hard-links, no-acls, no-xattrs
            `recursive`, `links`,
            `perms`, `chown=$USER:$USER`, // root:root
            `times`, `devices`, `specials`,
            `no-hard-links`, `no-acls`, `no-xattrs`,
            `verbose`,
        ].map(option => `--${option}`).join(' ');

        return this.exec(`rsync ${options} ${src} ${dest}`);
    }

    protected copySources(target: 'filesystem' | 'disk' | 'initrd') {
        this.mfs.rsync('src', `fs/${target}/`, this.paths.get('project.generated', target) + '/');
        Object
            .entries(this.mfs.resolveDir('src', `templates/fs/${target}/`, true))
            .forEach(([ relativeFilePath, filePath ]) => {
                relativeFilePath = relativeFilePath.replace(`templates/fs/${target}/`, '');
                this.app.templates.write(filePath, this.paths.get('project.generated', target, relativeFilePath));
            });
        let cfg = Config.makeProperty(new Config(this.app.config));
        Object.entries(this.app.config.copy[ target ])
            .map(([ src, dest ]) => {
                src  = cfg.process(src);
                dest = cfg.process(dest);
                return [ src, dest ];
            })
            .forEach(([ src, dest ]) => {
                dest = this.paths.get(`project.generated.${target}` as any, dest);
                ensureDirSync(dirname(dest));
                this.rsync(src, dest);
            });
    }
}


export interface TaskOptions {
    name: string;
    title?: string;
    description?: string;
    priority?: number
    depends?: Array<string | Task>;
}

function createTaskOptions(name: string, options: Partial<TaskOptions>): TaskOptions {
    return ({
        name,
        title      : name,
        description: '',
        priority   : 0,
        depends    : [],
        ...options,
    });
}


export class TaskArray<T extends Task = Task> extends Array<T> implements Array<T> {

    constructor(...items: T[]) {
        super(...items);
        Object.setPrototypeOf(this, new.target.prototype);
    }

    get(name: string): T {
        return this.find(item => item.name === name);
    }

    has(name: string): boolean {
        return this.get(name) !== undefined;
    }

    sorted() {
        const sorter = new DependencySorter();
        this
            .sort(firstBy('priority'))
            .forEach(task => {
                let deps = task.options.depends.map(dep => dep.toString());
                sorter.addItem(task.name, deps);
            });

        return new TaskArray(...sorter.sort().map(taskName => this.get(taskName)));
    }

    whereRequiredBy(task: string | Task) {
        return this.filter(item => item.options.depends.includes(task.toString()));
    }
}

