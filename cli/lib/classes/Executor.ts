import { merge } from 'lodash';
import { Application } from './Application';
import { Task, TaskArray, TaskOptions } from './Task';
import { inject, injectable } from 'inversify';

@injectable()
export class Executor {
    @inject('app') protected app: Application
    protected callbacks                   = {
        beforeRun: [],
        afterRun : [],
    };
    protected tasks: Record<string, Task> = {};

    addTask(name: string, cb: Function)
    addTask(name: string, options: Partial<TaskOptions>, cb: Function)
    addTask(task: Task)
    addTask(task: Task, overrides: Partial<TaskOptions>)
    addTask(...args): Task {
        let task: Task;
        if ( args[ 0 ] instanceof Task ) {
            task = args[ 0 ];
            if ( args[ 1 ] !== undefined ) {
                task.options = merge({}, task.options, args[ 1 ]);
            }
        } else {
            let name    = args[ 0 ].toString();
            let options = typeof args[ 1 ] === 'object' ? args[ 1 ] : {};
            let cb      = typeof args[ 1 ] === 'function' ? args[ 1 ] : args[ 2 ];
            task        = new Task(name, options, cb);
        }
        task.init(this.app, this);
        this.tasks[ task.name ] = task;
        return task;
    }

    getTasks() {
        return new TaskArray<Task>(...Object.values(this.tasks));
    }

    selectTasks(...taskNames: string[]) {
        return new TaskArray<Task>(...taskNames.map(taskName => this.tasks[ taskName ]));
    }

    async run(...runTaskNames: string[]) {
        let runTasks = this.selectTasks(...runTaskNames);
        let allTasks = this.getTasks().sorted();
        let tasks    = allTasks.filter(task => {
            // if task in true > true
            if ( runTaskNames.includes(task.name) ) {
                return true;
            }
            // if task is depended by any task of true > true
            let requiredBy = runTasks.whereRequiredBy(task.name);
            if ( requiredBy.length > 0 ) {
                return true;
            }
            return false;
        });

        for ( let task of tasks ) {
            for ( let cb of this.callbacks.beforeRun ) {
                await cb(task);
            }

            await task.run();

            for ( let cb of this.callbacks.afterRun ) {
                await cb(task);
            }
        }
    }

    beforeRun(cb: (task: Task) => any | Promise<any>) {
        this.callbacks.beforeRun.push(cb);
    }

    afterRun(cb: (task: Task) => any | Promise<any>) {
        this.callbacks.beforeRun.push(cb);
    }
}

