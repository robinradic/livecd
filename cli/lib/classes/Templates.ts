import { Application } from './Application';
import { Dictionary, merge, template, TemplateOptions } from 'lodash';
import { readFileSync, writeFileSync } from 'fs';
import { ensureDirSync } from '../utils/fs';
import { dirname } from 'path';
import { inject, injectable } from 'inversify';

@injectable()
export class Templates {
    globals: Dictionary<any> = {};

    @inject('app') protected app: Application;

    template(string: string, options: TemplateOptions = {}) {
        return template(string, merge(<TemplateOptions>{}, <TemplateOptions>{ imports: this.globals, interpolate: /<%=([\s\S]+?)%>/g }, options));
    }

    fileTemplate(src: string) {
        const source = readFileSync(src, 'utf8');
        return this.template(source);
    }

    write(src: string, dest: string, variables: any = {}) {
        this.app.log.verbose('%s: %s %s %s', 'template'.c('cyan'), src, '->'.c('grey'), dest);
        const tpl     = this.fileTemplate(src);
        const dirPath = dirname(dest);
        ensureDirSync(dirPath);
        writeFileSync(dest, tpl(variables), 'utf8');
        return this;
    }

    writeBatch(sources: string[], dest: string, variables: any = {}) {
        sources.forEach(source => this.write(source, dest, variables));
        return this;
    }

    setGlobals(globals: Dictionary<any>) {
        this.globals = globals;
        return this;
    }

    mergeGlobals(globals: Dictionary<any>) {
        this.globals = { ...this.globals, ...globals }; //merge(, globals);
        return this;
    }
}