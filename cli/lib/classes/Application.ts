import pkg from '../../package.json';
import { Config, loadDefaultConfig } from '../utils/config';
import { createOptions, Options as BaseOptions } from '../utils/options';
import { join, resolve } from 'path';
import { existsSync } from 'fs';
import { BasePaths, GlobalArguments, Newable, Paths } from '../interfaces';
import { Templates } from './Templates';
import { Bar, Options as BarOptions } from 'cli-progress';
import { Executor } from './Executor';
import chalk from 'chalk';
import { MirrorFS } from './MirrorFS';
import { Container, decorate, injectable, interfaces } from 'inversify';
import { container } from '../container';
import { logger, Logger } from '../utils/logger';
import { Command, CommandResult } from '../cli/commands/command';
import { CommandLineOptions } from 'command-line-args';
import { HelpCommand } from '../cli/commands/help';
import { InitCommand } from '../cli/commands/init';
import { ParsedArgs, Parser } from './Parser';
import { VersionCommand } from '../cli/commands/version';
import { globalArguments } from '../constants';
import { RunCommand } from '../cli/commands/run';
import { ChrootCommand } from '../cli/commands/chroot';
import { addTasks } from '../tasks';
import { ClearCacheCommand } from '../cli/commands/clear-cache';
import { ensureDirSync } from '../utils/fs';

export type PackageJSON = typeof pkg
export namespace Application {
    export interface Options {
        name?: string;
        description?: string;
        cwd?: string
        configFile?: string
        mode?: 'development' | 'production'
        argv?: string[]
        interactive?: boolean
        useCache?: boolean
    }

    export type OptionsProxy = BaseOptions<Options>
}

export class Application implements Command {
    protected _name = 'livecd';
    set name(name: string) {
        this._name    = name;
        process.title = name;
    }

    get name(): string {return this._name;}

    aliases     = [];
    description = 'Create a livecd';
    args        = globalArguments;
    globals: ParsedArgs<GlobalArguments>;

    config: Config;
    paths: Paths;
    options: Application.OptionsProxy;
    container: Container;
    commands: Map<string, Command> = new Map();

    get pkg(): PackageJSON {return require(this.paths[ 'cli.pkg' ]);}

    get log(): Logger { return this.get('log');}

    get parser(): Parser {return this.get('parser');};

    get mfs(): MirrorFS {return this.get('mfs');};

    get templates(): Templates {return this.get('templates');};

    get executor(): Executor {return this.get('executor');};

    constructor() {
        decorate(injectable(), Application);
        this.container = container;
        this.bind      = this.container.bind.bind(this.container);
        this.rebind    = this.container.rebind.bind(this.container);
        this.unbind    = this.container.unbind.bind(this.container);
        this.get       = this.container.get.bind(this.container);
        this.resolve   = this.container.resolve.bind(this.container);
        this.isBound   = this.container.isBound.bind(this.container);

        process.on('uncaughtException', (error: any) => {
            logger.error(error.toString());
            logger.debug(error.stack);
        });

        process.on('unhandledRejection', (error: any) => {
            logger.error(error.toString());
            logger.debug(error.stack);
        });

        if ( process.listenerCount('warning') === 0 ) {
            process.on('warning', (warning: any) => {
                logger.warn(warning);
                if ( warning.detail ) {
                    logger.error(warning.detail);
                }
            });
        }

        this.bind('app').toConstantValue(this);
        this.bind('log').toConstantValue(logger);
        this.bind('parser').to(Parser).inSingletonScope();
        this.bind('mfs').to(MirrorFS).inSingletonScope();
        this.bind('templates').to(Templates).inSingletonScope();
        this.bind('executor').to(Executor).inSingletonScope();

        this.addResolveCommand(InitCommand);
        this.addResolveCommand(RunCommand);
        this.addResolveCommand(ChrootCommand);
        this.addResolveCommand(ClearCacheCommand);
        this.addResolveCommand(VersionCommand);
        this.addResolveCommand(HelpCommand).setCommands(this.commands);


    }

    public async init(_options: Application.Options = {}) {
        this.bind('globals').toConstantValue(this.globals);

        this.options = createOptions<Application.Options>({
            cwd        : process.cwd(),
            mode       : 'development',
            argv       : _options.argv || process.argv.slice(2),
            interactive: true,
            useCache   : true,
        }, {
            configFile : configFile => {
                configFile = resolve(configFile);
                const mod  = require(configFile);
                this.config.merge(mod.default || mod);
                return configFile;
            },
            name       : value => this.name = value,
            description: value => this.description = value,
        });
        this.options.merge(_options);
        this.paths = this.resolvePaths(this.options.get('cwd', process.cwd()) as string);
        ensureDirSync(this.paths[ 'cli.tmp' ]);
        ensureDirSync(this.paths[ 'project.tmp' ]);
        this.config = loadDefaultConfig();
        if ( !this.options.configFile ) {
            this.options.configFile = this.guessConfigFile();
        }

        this.globals = this.parser.args<GlobalArguments>(
            globalArguments,
            _options.argv || process.argv.slice(2),
            {
                partial     : true,
                ignoreGroups: true,
            },
        );

        this.templates.mergeGlobals({
            config: this.config,
            app   : this,
        });

        this.mfs.defineMirror('src', this.paths.get('cli.src'), this.paths.get('project.src'));

        addTasks(this);

        return this;
    }

    public async execute(taskNames: string[]) {
        this.executor.beforeRun(task => this.log.info('%s', `${chalk.cyan.bold('task')}: ${chalk.blue.bold(task.options.title)} (${task.name}) :: ${chalk.grey(task.options.description)} `));
        await this.executor.run(...taskNames);
        return this;
    }

    public run = async (): Promise<CommandResult> => {
        const helpCommand  = this.commands.get('help')!;
        const commandNames = Array.from(this.commands.keys());
        let parsedArgs: CommandLineOptions;
        this.log.debug('running...');
        try {
            parsedArgs = this.parser.commands(commandNames, this.options.argv);
        } catch ( error ) {
            if ( error.command ) {
                this.log.warn(`'${error.command}' is not an available command.`);
            }
            return this.runCommand(helpCommand, { command: error.command });
        }

        const commandName = parsedArgs.command;
        const commandArgs = parsedArgs.argv;
        const command     = this.commands.get(commandName)!;
        if ( command == null ) {
            throw new TypeError('command is null');
        }
        this.log.debug(`command '${commandName}' found, parsing command args:`, { args: commandArgs });
        const commandDefinitions = this.parser.mergeArguments([ command.args, globalArguments ]);
        const commandOptions     = this.parser.args(commandDefinitions, commandArgs, { ignoreGroups: true });
        this.log.debug(`command options parsed from args:`, commandOptions);
        this.log.debug(`final project configuration generated:`);
        if ( commandOptions[ 'help' ] ) {
            this.log.debug(`'--help' option found, running 'help' for given command...`);
            return this.runCommand(helpCommand, { command: commandName });
        }

        this.log.debug('Running command...');
        return this.runCommand(command, commandOptions);
    };

    public progress(options: Partial<BarOptions> = {}): Bar {
        options                = {
            stopOnComplete : true,
            clearOnComplete: true,
            hideCursor     : true,
            stream         : process.stdout,
            barsize        : 100,
            ...options,
        };
        const { Bar, Presets } = require('cli-progress');
        return new Bar(options, Presets.shades_grey);
    }

    public addCommand(command: Command) {
        logger.debug('adding command', command.name);
        this.commands.set(command.name, command);

        command.aliases.forEach((alias) => {
            logger.debug('adding alias', alias);
            this.commands.set(alias, command);
        });
    }

    public addResolveCommand<T extends Command>(Command: Newable<T>): T {
        decorate(injectable(), Command);
        const command = this.resolve<T>(Command);
        this.addCommand(command);
        return command as T;
    }

    protected async runCommand(command: Command, ...params): Promise<CommandResult> {
        let result = await command.run(...params);
        if ( result instanceof CommandResult === false ) {
            result = new CommandResult(0);
        }
        return result as CommandResult;
    }

    protected guessConfigFile() {
        let possibleConfigFileNames = [
            'config.json',
            'config.js',
            'config.ts',
        ];
        for ( const possibleName of possibleConfigFileNames ) {
            if ( existsSync(resolve(possibleName)) ) {
                return resolve(possibleName);
            }
        }
    }

    protected resolvePaths(cwd: string = process.cwd()): Paths {
        let cli                   = resolve(__dirname, '../..');
        let _cli                  = (...parts: string[]) => resolve(cli, ...parts);
        let project               = cwd;
        let _project              = (...parts: string[]) => resolve(project, ...parts);
        let paths: Partial<Paths> = {
            cwd,
            cli,
            'cli.pkg'                        : _cli('package.json'),
            'cli.tmp'                        : _cli('.tmp'),
            'cli.lib'                        : _cli('lib'),
            'cli.src'                        : _cli('src'),
            'cli.src.fs'                     : _cli('src/fs'),
            'cli.src.fs.disk'                : _cli('src/fs/disk'),
            'cli.src.fs.filesystem'          : _cli('src/fs/filesystem'),
            'cli.src.fs.initrd'              : _cli('src/fs/initrd'),
            'cli.src.iso'                    : _cli('src/tessa.iso'),
            'cli.src.templates'              : _cli('src/templates'),
            'cli.src.templates.fs'           : _cli('src/templates/fs'),
            'cli.src.templates.fs.disk'      : _cli('src/templates/fs/disk'),
            'cli.src.templates.fs.filesystem': _cli('src/templates/fs/filesystem'),
            'cli.src.templates.fs.initrd'    : _cli('src/templates/fs/initrd'),
            project,
            'project.tmp'                    : _project('.tmp'),
            'project.src'                    : _project('src'),
            'project.src.disk'               : _project('src/disk'),
            'project.src.filesystem'         : _project('src/filesystem'),
            'project.src.initrd'             : _project('src/initrd'),
            'project.generated'              : _project('generated'),
            'project.generated.iso'          : _project('generated/custom.iso'),
            'project.generated.mnt'          : _project('generated/mnt'),
            'project.generated.disk'         : _project('generated/disk'),
            'project.generated.filesystem'   : _project('generated/filesystem'),
            'project.generated.initrd'       : _project('generated/initrd'),
        };
        paths.get                 = (name: keyof BasePaths, ...parts: string[]) => resolve(join(paths[ name ], ...parts));
        return paths as Paths;
    }


    bind: <T>(serviceIdentifier: interfaces.ServiceIdentifier<T>) => interfaces.BindingToSyntax<T>;
    rebind: <T>(serviceIdentifier: interfaces.ServiceIdentifier<T>) => interfaces.BindingToSyntax<T>;
    unbind: (serviceIdentifier: interfaces.ServiceIdentifier<any>) => void;
    get: <T>(serviceIdentifier: interfaces.ServiceIdentifier<T>) => T;
    resolve: <T>(constructorFunction: interfaces.Newable<T>) => T;
    isBound: (serviceIdentifier: interfaces.ServiceIdentifier<any>) => boolean;

}