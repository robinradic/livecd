import { Content as BaseContent, OptionDefinition, OptionList as BaseOptionList } from 'command-line-usage';
import { Application } from './classes/Application';

export interface ArgDefinition extends OptionDefinition {
    validate?: (value:any) => boolean
    transform?: (value:any, app:Application) => any
    handle?: (value:any, app:Application) => void
}

export interface Content extends BaseContent {}

export interface OptionList extends BaseOptionList {
    optionList?: ArgDefinition[]
}

export type Section = Content | OptionList

export interface Newable<T> {
    new(...args: any[]): T;
}

export interface GlobalArguments {
    production?: boolean
    verbose?: any
    help?: boolean
    quiet?: boolean
    root?: string
    config?: string
    noInteraction?: boolean
    noCache?: boolean
}


export interface BasePaths {
    cwd: string
    cli: string
    project: string
    'project.tmp': string
    'project.generated': string
    'project.generated.iso': string
    'project.generated.mnt': string
    'project.generated.disk': string
    'project.generated.filesystem': string
    'project.generated.initrd': string
    'project.src': string
    'project.src.disk': string
    'project.src.filesystem': string
    'project.src.initrd': string

    'cli.pkg': string
    'cli.tmp': string
    'cli.lib': string
    'cli.src': string
    'cli.src.fs': string
    'cli.src.fs.disk': string
    'cli.src.fs.filesystem': string
    'cli.src.fs.initrd': string
    'cli.src.iso': string
    'cli.src.templates': string
    'cli.src.templates.fs': string
    'cli.src.templates.fs.disk': string
    'cli.src.templates.fs.filesystem': string
    'cli.src.templates.fs.initrd': string
}

export interface Paths extends BasePaths {
    get(name: keyof BasePaths, ...parts: string[]): string
}

export interface IConfigCHRootItem {
    scripts?: Array<string>
    commands?: Array<string>
}

export interface IConfig {
    [ key: string ]: any

    name?: string
    version?: string
    title?: string
    username?: string
    livecd?: {
        username?: string
        userfullname?: string
        host?: string
    }
    copy?: {
        disk?: Record<string, string>
        filesystem?: Record<string, string>
        initrd?: Record<string, string>
    }
    chroot?: {
        filesystem?: Array<string>
        initrd?: Array<string>
    }
    paths?: Record<string, string>
}


