import chalk from 'chalk';

String.prototype.c = function (...colors: any[]) {
    let applyColor = [ chalk ].concat(colors).reduce((previousValue, currentValue) => previousValue[ currentValue.toString() ]) as any;
    return applyColor(this.toString());
} as any;
// Object.keys(Chalk).forEach(key => {
//     Object.defineProperty(String.prototype.c, key, {
//         configurable:true,
//         enumerable:true,
//         get(){
//             chalk[key](this.toString())
//             return this
//         }
//     });
// });
// var bind   = Function.bind;
// var unbind = bind.bind(bind);
//
// function instantiate(constructor, args) {
//     return new (unbind(constructor, null).apply(null, args));
// }
//
// function copyOwnPropertyDescriptors(From, To) {
//     var names = Object.getOwnPropertyNames(From);
//     // Loop through them
//     for ( var i = 0; i < names.length; i ++ ) {
//         // Skip props already in the MyDate object
//         if ( names[ i ] in To ) continue;
//         // Get property description from o
//         var desc = Object.getOwnPropertyDescriptor(From, names[ i ]);
//         // Use it to create property on MyDate
//         Object.defineProperty(To, names[ i ], desc);
//     }
// }
//
// String = function (String) {
//     copyOwnPropertyDescriptors(String, MyString);
//     MyString.prototype = String.prototype
//     const str = instantiate(MyString, arguments);
//
//     function MyString() {
//         return this;
//     }
//
// }(String) as any;

// proto.c = new Proxy(chalk, {
//     apply(target: Chalk, thisArg: any, argArray?: any): any {
//         let applyColor = [ chalk ].concat(argArray).reduce((previousValue, currentValue) => previousValue[ currentValue.toString() ]) as any;
//         return applyColor(thisArg.toString());
//     },
//     get(target: Chalk, p: string | number | symbol, receiver: any): any {
//
//         return target[ p ];
//     },
// }) as any;
// Object.setPrototypeOf(str, proto)

// String.prototype.c = function (...colors: any[]) {
//     let applyColor = [ chalk ].concat(colors).reduce((previousValue, currentValue) => previousValue[ currentValue.toString() ]) as any;
//     return applyColor(this.toString());
// } as any;
//
// let keys = ['grey','bold'];
// function createProxy (key){
//     return new Proxy(chalk[key], {
//         get(target: any, p: string | number | symbol, receiver: any): any {
//             if(keys.includes(p.toString())){
//                 return createProxy(p.toString())
//             }
//         },
//         apply(target: any, thisArg: any, argArray?: any): any {
//
//             return
//         },
//     })
// }
// ['grey','bold'].forEach(key => {
//     String.prototype.c[key] = createProxy(key)
// })
// // Object.defineProperty(String.prototype, 'c', {
// //     configurable:true,
// //     enumerable:true,
// //     get(){
// //         function fn(...colors:any){
// //             let applyColor = [ Chalk ].concat(colors).reduce((previousValue, currentValue) => previousValue[ currentValue.toString() ]) as any;
// //             return applyColor(this.toString());
// //         }
// //         return new Proxy(fn as any, {
// //             get(target: String, p: string | number | symbol, receiver: any): any {
// //                 if(chalk[p]) {
// //                     target = chalk[ p]
// //                 }
// //                 return target
// //             },
// //             apply(target: String, thisArg: any, argArray?: any): any {
// //                 return fn.apply(thisArg, argArray)
// //             },
// //         })
// //     }
// // });
// //
