import { ArgDefinition } from './interfaces';
import { Log } from './classes/Log';

export const globalArguments: ArgDefinition[] = ([
    {
        name        : 'production',
        alias       : 'p',
        description : 'Enables optimal production compression levels (increases build-time substantially)',
        type        : Boolean,
        defaultValue: false,
        handle      : (value: boolean, app) => {
            app.options.mode = value === true ? 'production' : 'development';
        },
    },
    {
        name        : 'verbose',
        description : 'turn on debugging output',
        lazyMultiple: true,
        type        : Boolean,
        alias       : 'v',
        handle      : (value: boolean[], app) => {
            let len          = value.filter(val => val === true).length;
            let levels       = Object.keys(app.log._log.winston.levels);
            let defaultLevel = app.log._log.winston.levels.info; // levels.findIndex(level => app.log._log.level === level);
            let newLevel     = levels[ defaultLevel + len ];
            if ( app.log._log.level !== newLevel ) {
                app.log._log.level = newLevel as Log.Level;
            }
        },
    },
    {
        name        : 'help',
        description : 'print out helpful usage information',
        type        : Boolean,
        alias       : 'h',
        defaultValue: false,
    },
    {
        name        : 'quiet',
        description : 'silence output',
        type        : Boolean,
        alias       : 'q',
        defaultValue: false,
    },
    {
        name       : 'no-interaction',
        description: 'Disable interactive prompts',
        type       : Boolean,
        alias      : 'i',
        handle     : (value: boolean, app) => {
            app.options.interactive = value !== true
        },
    },
    {
        name       : 'no-cache',
        description: 'Disable caching',
        type       : Boolean,
        handle     : (value: boolean, app) => {
            app.options.useCache = value !== true
        },
    },
    {
        name        : 'root',
        type        : String,
        description : 'The root directory of your project. Defaults to the current working directory.',
        defaultValue: process.cwd(),
    },
    {
        name       : 'config',
        type       : String,
        description: 'Path to a config.json/ts/js file. By default it searches the current working directory a match.',
    },
] as ArgDefinition[]).map(def => {
    def.group = 'global';
    return def;
});
