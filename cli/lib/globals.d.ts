import Enquirer = require('enquirer');
import { Choice, TransformedChoice } from 'enquirer';
import chalk from 'chalk';


type StringColor = {
    (...colors: (keyof typeof chalk)[]): string
}

declare global {
    interface String {
        c: StringColor
    }
}

declare module 'enquirer' {
    export interface Choice {
        name?: string
        message?: string
        hint?: string
        value?: any
        disabled?: boolean
    }

    export interface TransformedChoice extends Choice {
        normalized?: boolean
        input?: string
        index?: number
        cursor?: number
        parent?: any
        indent?: string
        path?: string
        enabled?: boolean
    }

    export class ArrayPrompt extends Enquirer.Prompt {

        initialize(): Promise<any>

        reset(): Promise<any>

        toChoices(value, parent?): Promise<TransformedChoice[]>

        toChoice(ele, i, parent): Promise<TransformedChoice>

        onChoice(choice, i)

        addChoice(ele, i, parent): TransformedChoice

        newItem(item, i, parent) // this.render
        indent(choice): string

        dispatch(s, key)

        focus(choice, enabled): Choice

        space() // this.render
        toggle(choice, enabled): Choice

        enable(choice): Choice

        disable(choice): Choice

        number(n): Promise<any>

        home() //this.render

        end() //this.render

        first() //this.render

        last() //this.render

        prev() //this.render

        next() //this.render

        right() //this.render

        left() //this.render

        up() //this.render

        down() //this.render

        scrollUp(i ?: number) //this.render

        scrollDown(i ?: number)//this.render

        shiftUp(): Promise<any>

        shiftDown(): Promise<any>

        pageUp() //this.render

        pageDown()//this.render

        swap(pos)

        isDisabled(choice ?): boolean

        isEnabled(choice ?): boolean

        isChoice(choice, value): boolean

        isSelected(choice): boolean

        map(names ?: string[], prop ?: string): any[]

        filter(value, prop): TransformedChoice[]

        find(value, prop): TransformedChoice

        findIndex(value): number

        submit(): Promise<any> //super.submit
        choices: Choice[];
        visible: boolean;
        limit: number;
        value: any;
        index: number;
        enabled: boolean;
        focused: Choice;
        selectable: Choice[];
        selected: Choice | Choice[];
    }

    export class Select extends ArrayPrompt {
        separator(): any

        pointer(choice, i): any

        indicator(choice, i): any
    }

    export class MultiSelect extends Select {

    }
}