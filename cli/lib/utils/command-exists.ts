const exec       = require('child_process').exec;
const execSync   = require('child_process').execSync;
const fs         = require('fs');
const path       = require('path');
const access     = fs.access;
const accessSync = fs.accessSync;
const constants  = fs.constants || fs;

const fileNotExists = function (commandName, callback) {
    access(commandName, constants.F_OK,
        function (err) {
            callback(!err);
        });
};

const fileNotExistsSync = function (commandName) {
    try {
        accessSync(commandName, constants.F_OK);
        return false;
    } catch ( e ) {
        return true;
    }
};

const localExecutable = function (commandName, callback) {
    access(commandName, constants.F_OK | constants.X_OK,
        function (err) {
            callback(null, !err);
        });
};

const localExecutableSync = function (commandName) {
    try {
        accessSync(commandName, constants.F_OK | constants.X_OK);
        return true;
    } catch ( e ) {
        return false;
    }
};

const commandExistsUnix = function (commandName, cleanedCommandName, callback) {

    fileNotExists(commandName, function (isFile) {

        if ( !isFile ) {
            const child = exec('command -v ' + cleanedCommandName +
                ' 2>/dev/null' +
                ' && { echo >&1 ' + cleanedCommandName + '; exit 0; }',
                function (error, stdout, stderr) {
                    callback(null, !!stdout);
                });
            return;
        }

        localExecutable(commandName, callback);
    });

};

const commandExistsUnixSync = function (commandName, cleanedCommandName): boolean {
    if ( fileNotExistsSync(commandName) ) {
        try {
            const stdout = execSync('command -v ' + cleanedCommandName +
                ' 2>/dev/null' +
                ' && { echo >&1 ' + cleanedCommandName + '; exit 0; }',
            );
            return !!stdout;
        } catch ( error ) {
            return false;
        }
    }
    return localExecutableSync(commandName);
};


let cleanInput = function (s) {
    if ( /[^A-Za-z0-9_\/:=-]/.test(s) ) {
        s = '\'' + s.replace(/'/g, '\'\\\'\'') + '\'';
        s = s.replace(/^(?:'')+/g, '') // unduplicate single-quote at the beginning
            .replace(/\\'''/g, '\\\''); // remove non-escaped single-quote if there are enclosed between 2 escaped
    }
    return s;
};

export function commandExists(commandName, callback) {
    const cleanedCommandName = cleanInput(commandName);
    if ( !callback && typeof Promise !== 'undefined' ) {
        return new Promise(function (resolve, reject) {
            commandExists(commandName, function (error, output) {
                if ( output ) {
                    resolve(commandName);
                } else {
                    reject(error);
                }
            });
        });
    }
    commandExistsUnix(commandName, cleanedCommandName, callback);
};

export function commandExistsSync(commandName) {
    const cleanedCommandName = cleanInput(commandName);
    return commandExistsUnixSync(commandName, cleanedCommandName);
};