import { execSync, ExecSyncOptionsWithStringEncoding } from 'child_process';
import { container } from '../container';
import { Application } from '../classes/Application';
import isRoot = require('is-root');


export function exec(command: string): string
export function exec(command: string, cwd: string): string
export function exec(command: string, options: Partial<ExecSyncOptionsWithStringEncoding>): string
export function exec(...args: any[]): string {
    let app     = container.get<Application>('app'),
        command = args.shift(),
        cwd     = app.paths.cwd,
        options = {};

    if ( args.length > 0 ) {
        if ( typeof args[ 0 ] === 'string' ) {
            cwd = args[ 0 ];
        } else if ( typeof args[ 0 ] === 'object' ) {
            options = args[ 0 ];
        }
    }

    app.log.verbose('%s', command, { label: 'exec' });
    let result = execSync(command, {
        encoding: 'utf8',
        cwd     : cwd,
        stdio   : 'pipe',
        ...options,
    });
    if ( result ) {
        app.log.debug(result, result);
    }
    return result;
}

export function sudo(command: string): string
export function sudo(command: string, cwd: string): string
export function sudo(command: string, options: Partial<ExecSyncOptionsWithStringEncoding>): string
export function sudo(...args: any[]): string {
    let command = args.shift().toString().trim();
    if ( !isRoot() ) {
        if ( !command.startsWith('sudo') ) {
            command = `sudo ${command}`;
        }
    }
    return exec(command, args[ 0 ]);
}