import { readdirSync, statSync } from 'fs';
import { join } from 'path';

import { copyFileSync, existsSync, mkdirSync, removeSync, unlinkSync } from 'fs-extra';

export function ensureCopyFileSync(src: string, dest: string, overwrite: boolean = false) {
    let exists = existsSync(dest);
    if ( overwrite && exists ) {
        unlinkSync(dest);
    }
    if ( !existsSync(dest) ) {
        copyFileSync(src, dest);
    }
}

export function ensureDirSync(path: string, clean = false, mode = 0o777) {
    if ( clean && existsSync(path) ) {
        removeSync(path);
    }
    if ( !existsSync(path) ) {
        mkdirSync(path, { recursive: true, mode });
    }
}


export function ensureRemoveSync(path: string) {
    if ( existsSync(path) ) {
        removeSync(path);
    }
}


export interface ListSyncOptions {
    recursive?: boolean
    files?: boolean
    directories?: boolean
}

export function listSync(dir: string, options: ListSyncOptions = {}) {
    options     = {
        recursive  : false,
        files      : true,
        directories: true,
        ...options,
    };
    let list    = [];
    const paths = readdirSync(dir);
    for ( let relativePath of paths ) {
        let path = join(dir, relativePath);
        let stat = statSync(path);
        if ( stat.isDirectory() ) {
            if ( options.directories ) {
                list.push(path);
            }
            if ( options.recursive ) {
                list = list.concat(listFilesSync(path, true));
            }
        } else if ( stat.isFile() ) {
            if ( options.files ) {
                list.push(path);
            }
        }
    }
    return list;
}

export function listFilesSync(dir: string, recursive: boolean = false) {
    return listSync(dir, { recursive, files: true, directories: false });
}

export function listDirsSync(dir: string, recursive: boolean = false) {
    return listSync(dir, { recursive, files: false, directories: true });
}
