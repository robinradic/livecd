import _, { get, has, merge, set, unset } from 'lodash';
import { Config } from '../classes/Config';

export interface OptionsFunctions<C> {
    get<T extends keyof C = keyof C>(path: T | string, defaultValue?: any): C[T]

    has<T extends keyof C = keyof C>(path: T | string): boolean

    merge<T extends keyof C = keyof C>(data: any): this

    set<T extends keyof C = keyof C>(path: T | string, value: any): this

    unset<T extends keyof C = keyof C>(path: T | string): this

}

export type Options<C> = C & OptionsFunctions<C>
export type OptionHandler = (value: any) => any

export interface IDelimitersCollection {
    [ index: string ]: IDelimiter;
}

export interface IDelimiterLodash {
    evaluate: RegExp;
    interpolate: RegExp;
    escape: RegExp;
}

export interface IDelimiter {
    opener?: string;
    closer?: string;
    lodash?: IDelimiterLodash;
}

const proxyMethods = [ 'get', 'has', 'set', 'unset', 'merge' ];
const proxySetters = [ 'set', 'unset', 'merge' ];

export function createOptions<C extends {} = any>(data: C, handlers: Partial<Record<keyof C, OptionHandler>> = {} as any): Options<C> {
    const options = new Proxy(data, {
        get(target: any, p: string | number | symbol, receiver: any): any {
            let key = p.toString();
            if ( proxyMethods.includes(key) ) {
                return (...params) => {
                    let result = _[ key ](target, ...params);
                    if ( proxySetters.includes(key) ) {
                        return receiver;
                    }
                    if ( key === 'get' ) {
                        result = Config.makeProperty(new Config(target)).get(params[ 0 ], params[ 1 ]);
                    }
                    return result;
                };
            }
            if ( target[ p ] ) {
                let result = Config.makeProperty(new Config(target)).get(key);
                return result;
            }
            return target[ p ];
        },
        set(target: C, p: string | number | symbol, value: any, receiver: any): boolean {
            let key = p.toString();
            if ( handlers[ key ] !== undefined ) {
                value = handlers[ key ](value);
            }
            target[p] = value
            return true
        },
    });
    return options;
}

