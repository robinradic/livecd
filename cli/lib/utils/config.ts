import _config from '../config.json';
import { createOptions, Options } from './options';
import { existsSync } from 'fs';
import { IConfig } from '../interfaces';

export type Config = Options<IConfig>

export function loadDefaultConfig(): Config {
    return createOptions<IConfig>(_config);
}

export function loadConfig(path: string): Config {
    if(existsSync(path)) {
        return createOptions<IConfig>(require(path));
    }
    return loadDefaultConfig()
}