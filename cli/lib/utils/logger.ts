import { Log } from '../classes/Log';


const _log = Log.createDefault();


export interface Logger {
    _log: Log

    (...args: any[]): Logger;

    dir(...args: any[]): Logger;

    error(...args: any[]): Logger;

    info(name: string, ...args: any[]): Logger;

    warn(...args: any[]): Logger;

    verbose(...args: any[]): Logger;

    debug(...args: any[]): Logger;

    silly(...args: any[]): Logger;
}


export let logger: Logger;

logger         = function (...args: any[]) {
    console.log(...args);
    return logger;
} as any;
logger._log    = _log;
logger.error   = function (...args: any[]) {
    logger._log.error(...args);
    return logger;
};
logger.warn    = function (...args: any[]) {
    logger._log.warn(...args);
    return logger;
};
logger.info    = function (...args: any[]) {
    logger._log.info(...args);
    return logger;
};
logger.verbose = function (...args: any[]) {
    logger._log.verbose(...args);
    return logger;
};
logger.debug   = function (...args: any[]) {
    logger._log.debug(...args);
    return logger;
};
logger.silly   = function (...args: any[]) {
    logger._log.silly(...args);
    return logger;
};
logger.dir     = function (...args: any[]) {
    console.dir.apply(console, ...args);
    return logger;
};


// logger.info    = function (name: string, ...args: any[]) {
//     logger._log.info(chalk.blueBright(name.trim()).trim() + ': ', ...args);
//     return logger;
// };
// logger.verbose = function (name:string, ...args: any[]) {
//     logger._log.verbose(chalk.blueBright(name.trim()).trim() + ': ', ...args);
//     return logger;
// };
// logger.debug   = function (name: string, ...args: any[]) {
//     logger._log.debug(chalk.blueBright(name.trim()).trim() + ': ', ...args);
//     return logger;
// };
// logger.silly   = function (name: string, ...args: any[]) {
//     logger._log.silly(chalk.blueBright(name.trim()).trim()+ ': ', ...args);
//     return logger;
// };