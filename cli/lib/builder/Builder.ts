export class Builder {
    public readonly hooks = {};
}

/*

builder
    Builder
    Plugin
    Stage
        Step


builder.stages.extract
builder.stages.extract.steps.disk({
    from: 'path/to/file.iso',
    to: 'extracted/disk',
    cache: true
})
builder.stages.extract.steps.filesystem({
    from: 'extracted/disk/casper/filesystem.squashfs',
    to: 'extracted/filesystem',
    cache: true
})



 */

class Stages {

}

class Stage {
    name: string;
}

class Steps {

}

class Step {
    name: string;
    done: boolean;
    depends: string[];
}

class ExtractDiskStep extends Step {
    name = 'disk';
}

class ExtractFilesystemStep extends Step {
    name = 'filesystem';
    depends = ['extract.filesystem']
}